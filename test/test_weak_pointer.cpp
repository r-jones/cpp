#include <catch2/catch_test_macros.hpp>

#include <memory/smart_pointers.h>
#include <test/test_base.h>

TEST_CASE("Constructor") {
    MockItem::reset();
    auto sp = shared_ptr<MockItem>{new MockItem()};
    SECTION ("Default") {
        auto wp = weak_ptr<MockItem>{};
        REQUIRE(wp.use_count() == 0);
        REQUIRE(wp.expired());
    }
    SECTION ("From shared_ptr") {
        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE(wp.use_count() == 1);
        REQUIRE_FALSE(wp.expired());
    }
    SECTION ("Copy constructor") {
        auto wp = weak_ptr<MockItem>{sp};
        auto wp_copy = weak_ptr<MockItem>{wp};
        REQUIRE(wp_copy.use_count() == 1);
        REQUIRE_FALSE(wp_copy.expired());
    }
    SECTION ("Move constructor") {
        auto moved_from_wp = weak_ptr<MockItem>{sp};
        auto moved_into_wp = weak_ptr<MockItem>{std::move(moved_from_wp)};
        REQUIRE(moved_into_wp.use_count() == 1);
        REQUIRE_FALSE(moved_into_wp.expired());
    }
    // TODO: Test template<class Y> constructors
}

TEST_CASE("operator=") {
    MockItem::reset();
    auto sp = shared_ptr<MockItem>{new MockItem()}, sp2 = sp;
    SECTION ("Copy assignment") {
        REQUIRE(sp.use_count() == 2);

        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp.use_count() == 2);

        auto sp3 = shared_ptr<MockItem>{new MockItem()};
        auto wp2 = weak_ptr<MockItem>{sp3};
        REQUIRE(wp2.use_count() == 1);

        wp = wp2;
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp2.use_count() == 1);
        REQUIRE(wp.use_count() == 1);
    }
    SECTION ("Assign from shared ptr") {
        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp.use_count() == 2);

        auto sp3 = shared_ptr<MockItem>{new MockItem()};
        wp = sp3;
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp.use_count() == 1);
    }
    SECTION ("Move assignment") {
        REQUIRE(sp.use_count() == 2);

        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp.use_count() == 2);

        auto sp3 = shared_ptr<MockItem>{new MockItem()};
        auto wp2 = weak_ptr<MockItem>{sp3};
        REQUIRE(wp2.use_count() == 1);

        wp = std::move(wp2);
        REQUIRE(sp.use_count() == 2);
        REQUIRE(wp.use_count() == 1);
    }
}

TEST_CASE("Expired") {
    MockItem::reset();
    SECTION ("Default ") {
        auto wp = weak_ptr<MockItem>{};
        REQUIRE(wp.expired());
    }
    SECTION ("Expire only after object destroyed") {
        auto sp = shared_ptr<MockItem>{new MockItem()}, sp2 = sp;
        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE_FALSE(wp.expired());
        sp.reset();
        REQUIRE_FALSE(wp.expired());
        sp2.reset();
        REQUIRE(wp.expired());
    }
}

TEST_CASE("Lock") {
    MockItem::reset();
    SECTION ("Lock false if no object managed") {
        auto wp = weak_ptr<MockItem>{};
        REQUIRE_FALSE(wp.lock());
    }
    SECTION ("Lock true") {
        auto sp = shared_ptr<MockItem>{new MockItem()};
        auto wp = weak_ptr<MockItem>{sp};
        REQUIRE(wp.lock() == sp);
    }
    SECTION ("Lock false after object destroyed") {
        auto sp = shared_ptr<MockItem>{new MockItem()};
        auto wp = weak_ptr<MockItem>{sp};
        sp.reset();
        REQUIRE_FALSE(wp.lock());
    }
}

TEST_CASE("Reset") {
    MockItem::reset();
    auto sp = shared_ptr<MockItem>{new MockItem()}, sp2 = sp, sp3 = sp;
    auto wp = weak_ptr<MockItem>{sp};
    REQUIRE(sp.use_count() == 3);
    REQUIRE(wp.use_count() == 3);
    REQUIRE_FALSE(wp.expired());

    wp.reset();
    REQUIRE(sp.use_count() == 3);
    REQUIRE(wp.use_count() == 0);
    REQUIRE(wp.expired());
}

TEST_CASE("Swap") {
    MockItem::reset();
    auto sp1 = shared_ptr<MockItem>{new MockItem(100)};
    auto sp2 = shared_ptr<MockItem>{new MockItem(200)};
    auto wp1 = weak_ptr<MockItem>{sp1};
    auto wp2 = weak_ptr<MockItem>{sp2};
    REQUIRE(wp1.lock()->value == 100);
    REQUIRE(wp2.lock()->value == 200);

    wp1.swap(wp2);
    REQUIRE(wp1.lock()->value == 200);
    REQUIRE(wp2.lock()->value == 100);

    wp1.reset();
    REQUIRE_FALSE(wp1.lock());
    REQUIRE(wp2.lock()->value == 100);

    wp1.swap(wp2);
    REQUIRE(wp1.lock()->value == 100);
    REQUIRE_FALSE(wp2.lock());
}
