#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <test/test_base.h>
#include <util/util.h>


TEST_CASE("Swap") {
    MockItem a{1};
    MockItem b{2};

    MockItem::reset();
    swap(a, b);

    REQUIRE(a.value == 2);
    REQUIRE(b.value == 1);
    REQUIRE(MockItem::move_constructed == 1);
    REQUIRE(MockItem::move_assigned == 2);
}

TEST_CASE("Exchange") {
    MockItem a{1};

    MockItem::reset();
    auto b = exchange(a, 2);
    REQUIRE(a.value == 2);
    REQUIRE(b.value == 1);
    REQUIRE(MockItem::move_constructed == 1);
    REQUIRE(MockItem::move_assigned == 1);
}

TEST_CASE("Iterswap") {
    std::vector<MockItem> vec{1, 2};

    MockItem::reset();
    iter_swap(vec.begin(), std::next(vec.begin()));
    REQUIRE(vec.front() == 2);
    REQUIRE(vec.back() == 1);
    REQUIRE(MockItem::move_constructed == 1);
    REQUIRE(MockItem::move_assigned == 2);
}