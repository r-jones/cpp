#include <catch2/catch_test_macros.hpp>

#include <memory/smart_pointers.h>
#include <test/test_base.h>


TEST_CASE("Constructor") {
    MockItem::reset();
    auto ptr = new MockItem();
    SECTION ("Default") {
        auto sp = shared_ptr<MockItem>{};
        REQUIRE(sp.get() == nullptr);
        REQUIRE(sp.use_count() == 0);
        REQUIRE_FALSE(sp);
    }
    SECTION ("From object ptr") {
        auto sp = shared_ptr<MockItem>{ptr};
        REQUIRE(sp.get() == ptr);
        REQUIRE(sp.use_count() == 1);
        REQUIRE(sp);
    }
    SECTION ("Copy constructor") {
        auto sp = shared_ptr<MockItem>{ptr};
        auto sp_copy = shared_ptr<MockItem>{sp};
        REQUIRE(sp.use_count() == 2);
        REQUIRE(sp_copy.get() == ptr);
        REQUIRE(sp_copy.use_count() == 2);
        REQUIRE(sp_copy);
    }
    SECTION ("Move constructor") {
        auto moved_from_sp = shared_ptr<MockItem>{ptr};
        auto moved_into_sp = shared_ptr<MockItem>{std::move(moved_from_sp)};
        REQUIRE(moved_into_sp.use_count() == 1);
        REQUIRE(moved_into_sp.get() == ptr);
        REQUIRE(moved_into_sp);
    }
    SECTION ("From weak_ptr") {
        auto sp = shared_ptr<MockItem>{ptr};
        auto wp = weak_ptr<MockItem>{sp};
        auto sp_from_wp = shared_ptr<MockItem>{wp};

        REQUIRE(sp_from_wp.use_count() == 2);
        REQUIRE(sp_from_wp.get() == ptr);
    }
    SECTION ("Move constructed from unique_ptr") {
        auto up = make_unique<MockItem>();
        auto ptr = up.get();
        auto sp = shared_ptr<MockItem>(std::move(up));
        REQUIRE(sp.use_count() == 1);
        REQUIRE(sp.get() == ptr);
        REQUIRE(sp);
        REQUIRE_FALSE(up);
    }
}

TEST_CASE("operator=") {
    MockItem::reset();
    auto ptr = new MockItem();
    auto other_ptr = new MockItem();
    SECTION ("Self assignment") {
        auto copied_to_sp = shared_ptr<MockItem>{ptr};
        copied_to_sp = copied_to_sp;
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(copied_to_sp);
    }
    SECTION ("Copy assignment") {
        auto copied_to_sp = shared_ptr<MockItem>{ptr};
        auto copied_from_sp = shared_ptr<MockItem>{other_ptr};
        copied_to_sp = copied_from_sp;

        REQUIRE(MockItem::destructed == 1);
        REQUIRE(copied_to_sp.get() == other_ptr);
        REQUIRE(copied_to_sp.use_count() == 2);
    }
    SECTION ("Move assignment") {
        auto moved_to_sp = shared_ptr<MockItem>{ptr};
        auto moved_from_sp = shared_ptr<MockItem>{other_ptr};
        moved_to_sp = std::move(moved_from_sp);

        REQUIRE(MockItem::destructed == 1);
        REQUIRE(moved_to_sp.use_count() == 1);
        REQUIRE(moved_to_sp.get() == other_ptr);
    }
    SECTION ("Move assigned from unique_ptr") {
        auto up = make_unique<MockItem>();
        auto ptr = up.get();

        auto moved_to_sp = shared_ptr<MockItem>{new MockItem()};
        moved_to_sp = std::move(up);

        REQUIRE(MockItem::destructed == 1);
        REQUIRE(moved_to_sp.use_count() == 1);
        REQUIRE(moved_to_sp.get() == ptr);
    }
}

TEST_CASE("Destructor / Use Count") {
    MockItem::reset();
    auto ptr = new MockItem();
    {
        auto sp1 = shared_ptr<MockItem>{ptr};
        {
            auto sp2 = sp1;
            {
                auto sp3 = sp2;
                REQUIRE(sp1.use_count() == 3);
            }
            REQUIRE(sp1.use_count() == 2);
            REQUIRE(MockItem::destructed == 0);
        }
        REQUIRE(sp1.use_count() == 1);
        REQUIRE(MockItem::destructed == 0);
    }
    REQUIRE(MockItem::destructed == 1);
}

TEST_CASE("Reset") {
    MockItem::reset();
    auto ptr = new MockItem();
    auto other_ptr = new MockItem();
    auto sp = shared_ptr<MockItem>{ptr};
    SECTION ("Destroy object after final reset") {
        sp.reset(other_ptr);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(sp.get() == other_ptr);
        REQUIRE(sp.use_count() == 1);
    }
    SECTION ("Maintain object") {
        auto sp2 = sp;
        sp.reset(other_ptr);
        REQUIRE(MockItem::destructed == 0);
    }
}

TEST_CASE("Swap") {
    auto ptr = new MockItem();
    auto sp = shared_ptr<MockItem>{ptr};
    auto sp2 = sp;

    auto other_ptr = new MockItem();
    auto other_sp = shared_ptr<MockItem>{other_ptr};

    sp.swap(other_sp);

    REQUIRE(sp.use_count() == 1);
    REQUIRE(sp.get() == other_ptr);
    REQUIRE(other_sp.use_count() == 2);
    REQUIRE(other_sp.get() == ptr);
}

TEST_CASE("operator* / operator->") {
    auto sp = shared_ptr<MockItem>{new MockItem()};
    REQUIRE((*sp).value == 0);
    REQUIRE(sp->value == 0);
}

TEST_CASE("operator bool") {
    SECTION ("False with no managed object") {
        auto sp = shared_ptr<MockItem>{};
        REQUIRE_FALSE(sp);
    }
    SECTION ("True with managed object") {
        auto sp = shared_ptr<MockItem>{new MockItem()};
        REQUIRE(sp);
    }
}

TEST_CASE("make_shared") {
    MockItem::reset();
    auto sp = make_shared<MockItem>(1);
    REQUIRE(sp);
    REQUIRE(sp->value == 1);
    REQUIRE(MockItem::constructed == 1);
    REQUIRE(MockItem::destructed == 0);
}
