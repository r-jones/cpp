#include <catch2/catch_test_macros.hpp>

#include <containers/vector.h>
#include <test/test_base.h>


TEST_CASE("Constructor") {
    MockItem::reset();
    SECTION("Default") {
        vector<MockItem> vec(3);
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 0 && vec[1] == 0 && vec[2] == 0));
        REQUIRE(MockItem::constructed == 3);
    }
    SECTION("N copies of value") {
        vector<MockItem> vec(3, 1);
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 1 && vec[1] == 1 && vec[2] == 1));
        REQUIRE(MockItem::copy_constructed == 3);
    }
    SECTION("Fill from array") {
        int arr[] = {1, 2, 3};
        vector<MockItem> vec(arr, arr + 3);
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 1 && vec[1] == 2 && vec[2] == 3));
        REQUIRE(MockItem::constructed == 3);
    }
    SECTION("Initializer list") {
        vector<MockItem> vec{1, 2, 3};
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 1 && vec[1] == 2 && vec[2] == 3));
        REQUIRE(MockItem::copy_constructed == 3);
    }
    SECTION("Copy constructor") {
        vector<MockItem> other{1, 2, 3};

        MockItem::reset();
        vector<MockItem> vec{other};
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 1 && vec[1] == 2 && vec[2] == 3));
        REQUIRE(other == vec);
        REQUIRE(MockItem::copy_constructed == 3);
    }
    SECTION("Move constructor") {
        vector<MockItem> other{1, 2, 3};

        MockItem::reset();
        vector<MockItem> vec{std::move(other)};
        REQUIRE(vec.size() == 3);
        REQUIRE((vec[0] == 1 && vec[1] == 2 && vec[2] == 3));
        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
    }

    SECTION("Move constructor with allocator (different)") {
        // TODO: Write this test
    }
}

TEST_CASE("Destructor") {
    MockItem::reset();
    {
        vector<MockItem> vec(3);
    }
    REQUIRE(MockItem::destructed == 3);
}

TEST_CASE("operator=") {
    vector<MockItem> orig{1, 2};
    vector<MockItem> smaller{7};
    vector<MockItem> larger{4, 5, 6};
    MockItem::reset();
    SECTION ("Copy assign (from smaller)") {
        orig = smaller;
        REQUIRE(orig.size() == 1);
        REQUIRE(orig[0] == 7);
        REQUIRE(orig == smaller);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 1);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION ("Copy assign (from larger)") {
        orig = larger;
        REQUIRE(orig.size() == 3);
        REQUIRE((orig[0] == 4 && orig[1] == 5 && orig[2] == 6));
        REQUIRE(orig == larger);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 2);
        REQUIRE(MockItem::copy_constructed == 3);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION ("Move assign (from smaller)") {
        orig = std::move(smaller);
        REQUIRE(orig.size() == 1);
        REQUIRE(orig[0] == 7);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 1);
    }
    SECTION ("Move assign (from larger)") {
        orig = std::move(larger);
        REQUIRE(orig.size() == 3);
        REQUIRE((orig[0] == 4 && orig[1] == 5 && orig[2] == 6));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 2);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 3);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION ("Initializer list assign"){
        orig = {4, 5, 6};
        REQUIRE(orig.size() == 3);
        REQUIRE((orig[0] == 4 && orig[1] == 5 && orig[2] == 6));

        REQUIRE(MockItem::constructed == 3);
        REQUIRE(MockItem::destructed == 5);
        REQUIRE(MockItem::copy_constructed == 3);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
}

TEST_CASE("assign") {
    vector<MockItem> a{1, 2, 3, 4};
    MockItem::reset();
    SECTION ("From values (smaller)") {
        a.assign(3, 1);
        REQUIRE(a.size() == 3);
        REQUIRE((a[0] == 1 && a[1] == 1 && a[2] == 1));

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::destructed == 2);
        REQUIRE(MockItem::copy_assigned == 3);
        REQUIRE(MockItem::copy_constructed == 0);
    }
    SECTION ("From values (larger)") {
        a.assign(5, 1);
        REQUIRE(a.size() == 5);
        REQUIRE((a[0] == 1 && a[1] == 1 && a[2] == 1 && a[3] == 1 && a[4] == 1));

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::destructed == 5);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::copy_constructed == 5);
    }
    SECTION ("From iterator (smaller)") {
        int arr[] = {4, 5, 6};
        a.assign(arr, arr + 3);
        REQUIRE(a.size() == 3);
        REQUIRE((a[0] == 4 && a[1] == 5 && a[2] == 6));

        REQUIRE(MockItem::constructed == 3);
        REQUIRE(MockItem::destructed == 4);
        REQUIRE(MockItem::move_assigned == 3);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
    }
    SECTION ("From iterator (larger)") {
        int arr[] = {4, 5, 6, 7, 8};
        a.assign(arr, arr + 5);
        REQUIRE(a.size() == 5);
        REQUIRE((a[0] == 4 && a[1] == 5 && a[2] == 6 && a[3] == 7 && a[4] == 8));

        REQUIRE(MockItem::constructed == 5);
        REQUIRE(MockItem::destructed == 4);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::copy_constructed == 0);
    }
    SECTION ("From initializer list") {
        a.assign({1});
        REQUIRE(a.size() == 1);
        REQUIRE(a[0] == 1);

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::destructed == 4);
        REQUIRE(MockItem::copy_assigned == 1);
        REQUIRE(MockItem::copy_constructed == 0);
    }
}

TEST_CASE("at") {
    vector<MockItem> a{1, 2};
    REQUIRE((a.at(0) == 1 && a.at(1) == 2));
    REQUIRE_THROWS(a.at(2));
}

TEST_CASE("operator[]") {
    vector<MockItem> a{1, 2};
    REQUIRE((a[0] == 1 && a[1] == 2));
}

TEST_CASE("front") {
    vector<MockItem> a{1, 2};
    REQUIRE(a.front() == 1);
}

TEST_CASE("back") {
    vector<MockItem> a{1, 2};
    REQUIRE(a.back() == 2);
}

TEST_CASE("empty") {
    vector<MockItem> a{};
    REQUIRE(a.empty());

    a.assign({1, 2});
    REQUIRE(!a.empty());
}

TEST_CASE("reserve") {
    MockItem::reset();
    vector<MockItem> a{};
    a.reserve(100);
    REQUIRE(a.capacity() >= 100);
    REQUIRE(MockItem::constructed == 0);
}

TEST_CASE("clear") {
    MockItem::reset();
    vector<MockItem> a(2);
    a.clear();
    REQUIRE(a.empty());
    REQUIRE(a.capacity() >= 2);
    REQUIRE(MockItem::destructed == 2);
}

TEST_CASE("insert") {
    vector<MockItem> a{1, 2, 3};
    MockItem::reset();
    SECTION ("Single element rvalue") {
        a.insert(a.begin() + 1, 10);
        REQUIRE(a.size() == 4);
        REQUIRE((a[0] == 1 && a[1] == 10 && a[2] == 2 && a[3] == 3));

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_constructed == 2); // 1 move to the end (uninitialized memory), 1 for new item
        REQUIRE(MockItem::move_assigned == 1); // 1 move of middle values
    }
    SECTION ("Single element lvalue") {
        MockItem item{10};
        a.insert(a.begin() + 1, item);
        REQUIRE(a.size() == 4);
        REQUIRE((a[0] == 1 && a[1] == 10 && a[2] == 2 && a[3] == 3));

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::copy_constructed == 1); // new item
        REQUIRE(MockItem::move_constructed == 1); // move to the end (uninitialized memory)
        REQUIRE(MockItem::move_assigned == 1); // 1 move of middle values
    }
    SECTION ("Two elements") {
        a.insert(a.begin(), 2, 10);
        REQUIRE(a.size() == 5);
        REQUIRE((a[0] == 10 && a[1] == 10 && a[2] == 1 && a[3] == 2 && a[4] == 3));

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::move_constructed == 2); // 2 moves to the end (uninitialized memory)
        REQUIRE(MockItem::move_assigned == 1); // 1 moves of middle items
        REQUIRE(MockItem::copy_constructed == 2); // new items
    }
    SECTION ("From iterator") {
        int arr[] = {5, 6};
        a.insert(a.begin() + 1, arr, arr + 2);
        REQUIRE(a.size() == 5);
        REQUIRE((a[0] == 1 && a[1] == 5 && a[2] == 6 && a[3] == 2 && a[4] == 3));

        REQUIRE(MockItem::constructed == 2);
        REQUIRE(MockItem::move_constructed == 2);  // 2 moves to the end (uninitialized memory)
        REQUIRE(MockItem::move_assigned == 0);  // 0 moves of middle items
    }
    SECTION ("From initializer list") {
        a.insert(a.begin() + 1, {5, 6});
        REQUIRE(a.size() == 5);
        REQUIRE((a[0] == 1 && a[1] == 5 && a[2] == 6 && a[3] == 2 && a[4] == 3));

        REQUIRE(MockItem::constructed == 2);
        REQUIRE(MockItem::move_constructed == 2);  // 2 moves to the end (uninitialized memory)
        REQUIRE(MockItem::move_assigned == 0);  // 0 moves of middle items
        REQUIRE(MockItem::copy_constructed == 2); // new items
    }
}

TEST_CASE("emplace") {
    vector<MockItem> a{1, 2, 3};
    MockItem::reset();

    a.emplace(a.begin() + 1, 10);
    REQUIRE(a.size() == 4);
    REQUIRE((a[0] == 1 && a[1] == 10 && a[2] == 2 && a[3] == 3));

    REQUIRE(MockItem::move_constructed == 1);
    REQUIRE(MockItem::move_assigned == 1);
    REQUIRE(MockItem::constructed == 1);
}

TEST_CASE("erase") {
    MockItem::reset();
    SECTION ("First element") {
        vector<MockItem> a{1, 2, 3};
        MockItem::reset();
        a.erase(a.begin());
        REQUIRE(a.size() == 2);
        REQUIRE((a[0] == 2 && a[1] == 3));

        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::move_assigned == 2);
    }
    SECTION ("Over range") {
        vector<MockItem> a{1, 2, 3, 4};
        MockItem::reset();
        a.erase(a.begin() + 1, a.begin() + 3);
        REQUIRE(a.size() == 2);
        REQUIRE((a[0] == 1 && a[1] == 4));

        REQUIRE(MockItem::destructed == 2);
        REQUIRE(MockItem::move_assigned == 1);
    }
}

TEST_CASE("push_back") {
    vector<MockItem> a{1, 2, 3};
    MockItem::reset();
    SECTION ("lvalue") {
        MockItem value{1};
        a.push_back(value);
        REQUIRE(a.size() == 4);
        REQUIRE(a[3] == 1);

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::copy_constructed == 1);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION ("rvalue") {
        a.push_back(4);
        REQUIRE(a.size() == 4);
        REQUIRE(a[3] == 4);

        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 1);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
}

TEST_CASE("emplace_back") {
    vector<MockItem> a{1, 2, 3};
    MockItem::reset();
    a.emplace_back(4);
    REQUIRE(a.size() == 4);
    REQUIRE(a[3] == 4);

    REQUIRE(MockItem::constructed == 1);
    REQUIRE(MockItem::move_constructed == 0);
    REQUIRE(MockItem::copy_constructed == 0);
    REQUIRE(MockItem::copy_assigned == 0);
    REQUIRE(MockItem::move_assigned == 0);
}

TEST_CASE("pop_back") {
    vector<MockItem> a{1, 2, 3};
    MockItem::reset();
    a.pop_back();
    REQUIRE(a.size() == 2);
    REQUIRE(a[1] == 2);

    REQUIRE(MockItem::destructed == 1);
}

TEST_CASE("resize") {
    vector<MockItem> a{1, 2};
    MockItem::reset();
    SECTION ("Resize smaller") {
        a.resize(1);
        REQUIRE(a.size() == 1);
        REQUIRE(a[0] == 1);

        REQUIRE(MockItem::destructed == 1);
    }
    SECTION ("Resize bigger with default value") {
        a.resize(4);
        REQUIRE(a.size() == 4);
        REQUIRE((a[2] == 0 && a[3] == 0));

        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::constructed == 2);

    }
    SECTION ("Resize bigger with given value") {
        a.resize(4, 10);
        REQUIRE(a.size() == 4);
        REQUIRE((a[2] == 10 && a[3] == 10));

        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 2);
    }
}

TEST_CASE("swap") {
    vector<MockItem> a{1, 2};
    vector<MockItem> b{3, 4, 5};
    MockItem::reset();

    a.swap(b);
    REQUIRE(a.size() == 3);
    REQUIRE(b.size() == 2);
    REQUIRE((a[0] == 3 && a[1] == 4 && a[2] == 5));
    REQUIRE((b[0] == 1 && b[1] == 2));

    REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
}





