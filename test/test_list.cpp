#include <catch2/catch_test_macros.hpp>
#include <iostream>

//#include <containers/list.h>
#include <memory/allocator.h>
#include <test/test_base.h>

#include <list>
#include <vector>
#include <containers/list.h>

template<typename T>
using allocator = std::allocator<T>;
//using allocator = arena_allocator<T>; // std::allocator<T>>

//template<typename T, typename Allocator = allocator<T>>
//using list = std::list<T, Allocator>;
//using list = list<T, Allocator>;


TEST_CASE("Constructor") {
    std::vector<MockItem> vec{1, 2};
    MockItem::reset();
    SECTION("Default") {
        list<MockItem> lst{};
        REQUIRE(lst.size() == 0);
        REQUIRE(lst.get_allocator() == allocator<MockItem>{});
        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
    }
    SECTION("From count & value") {
        list<MockItem> lst(2, MockItem{1});
        REQUIRE(lst.size() == 2);
        REQUIRE((lst.front() == 1 && lst.back() == 1));
        REQUIRE(lst.get_allocator() == allocator<MockItem>{});
        REQUIRE(MockItem::constructed == 1);
        REQUIRE(MockItem::copy_constructed == 2);
    }
    SECTION("From count") {
        list<MockItem> lst(2);
        REQUIRE(lst.size() == 2);
        REQUIRE((lst.front() == 0 && lst.back() == 0));
        REQUIRE(lst.get_allocator() == allocator<MockItem>{});
        REQUIRE(MockItem::constructed == 2);
        REQUIRE(MockItem::copy_constructed == 0);
    }
    SECTION("From Iterator") {
        list<MockItem> lst{vec.begin(), vec.end()};
        REQUIRE(lst.size() == 2);
        REQUIRE((lst.front() == 1 && lst.back() == 2));
        REQUIRE(MockItem::copy_constructed == 2);
    }
    SECTION("From initializer list") {
        list<MockItem> lst{1, 2};
        REQUIRE(lst.size() == 2);
        REQUIRE((lst.front() == 1 && lst.back() == 2));
        REQUIRE(MockItem::constructed == 2);
        REQUIRE(MockItem::copy_constructed == 2);
    }
}

//template<typename T>
//class TD;
//
//namespace __detail
//{
//    template<typename _Tp>
//    using __with_ref = _Tp&;
//
//    template<typename _Tp>
//    concept __can_reference = requires { typename __with_ref<_Tp>; };
//
//    template<typename _Tp>
//    concept __dereferenceable = requires(_Tp& __t)
//    {
//        { *__t } -> __can_reference;
//    };
//}
//
//template<__detail::__dereferenceable _Tp>
//using iter_reference_t = decltype(*std::declval<_Tp&>());

//std::list lst;

TEST_CASE("Copy Constructor") {
    list<MockItem, MockAllocator> copied_from{{1, 2, 3}, MockAllocator{1}};
    MockItem::reset();
    SECTION("No allocator") {
        list<MockItem, MockAllocator> lst{copied_from};

        REQUIRE(lst.size() == 3);
        REQUIRE((lst.front() == 1 && lst.back() == 3));
        REQUIRE(lst == copied_from);

        REQUIRE(lst.get_allocator() ==
                    MockAllocatorTraits::select_on_container_copy_construction(copied_from.get_allocator()));
        REQUIRE(MockItem::copy_constructed == 3);
    }
    SECTION("With allocator") {
        MockAllocator alloc{2};
        list<MockItem, decltype(alloc)> lst(copied_from, alloc);
        REQUIRE(lst.size() == 3);
        REQUIRE((lst.front() == 1 && lst.back() == 3));
        REQUIRE(lst == copied_from);

        REQUIRE(lst.get_allocator() == MockAllocator{2});
        REQUIRE(MockItem::copy_constructed == 3);
    }
}

TEST_CASE("Move Constructor") {
    list<MockItem, MockAllocator> moved_from{{1, 2, 3}, MockAllocator{1}};
    MockItem::reset();
    SECTION("No allocator") {
        list<MockItem, MockAllocator> lst{std::move(moved_from)};

        REQUIRE(lst.size() == 3);
        REQUIRE((lst.front() == 1 && lst.back() == 3));
        REQUIRE(lst.get_allocator() == moved_from.get_allocator());

        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
    }
    SECTION("Same allocator") {
        list<MockItem, MockAllocator> lst{std::move(moved_from), MockAllocator{1}};

        REQUIRE(lst.size() == 3);
        REQUIRE((lst.front() == 1 && lst.back() == 3));
        REQUIRE(lst.get_allocator() == MockAllocator{1});

        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
    }
    SECTION("Different allocator") {
        list<MockItem, MockAllocator> lst{std::move(moved_from), MockAllocator{2}};

        REQUIRE(lst.size() == 3);
        REQUIRE((lst.front() == 1 && lst.back() == 3));
        REQUIRE(lst.get_allocator() == MockAllocator{2});

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 3);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
}

TEST_CASE("Destructor") {
    MockItem::reset();
    SECTION("Destroy elements") {
        {
            list<MockItem> lst(3);
            REQUIRE(MockItem::destructed == 0);
        }
        REQUIRE(MockItem::destructed == 3);
    }
    SECTION("Pointers as elements (don't destroy)") {
        MockItem item1 = 1, item2 = 2;
        {
            list<MockItem*> lst{&item1, &item2};
            REQUIRE(MockItem::destructed == 0);
        }
        REQUIRE(MockItem::destructed == 0);
    }
}

TEST_CASE("Assignment") {
    list<MockItem, MockAllocator> assigned_to{1, 2, MockAllocator{1}};
    list<MockItem, MockAllocator> assigned_from{{4, 10, 5}, MockAllocator{1}};

    list<MockItem, MockAllocatorWithoutPOCAPOMA>
            assigned_to_without_poca_poma{1, 2, MockAllocatorWithoutPOCAPOMA{1}};

    list<MockItem, MockAllocatorWithoutPOCAPOMA>
            lst_diff_allocator_without_poca_poma{{4, 10, 5}, MockAllocatorWithoutPOCAPOMA{2}};

    list<MockItem, MockAllocatorWithPOCAPOMA>
            assigned_to_with_poca_poma{1, 2, MockAllocatorWithPOCAPOMA{1}};

    list<MockItem, MockAllocatorWithPOCAPOMA>
            lst_diff_allocator_with_poca_poma{{4, 10, 5}, MockAllocatorWithPOCAPOMA{2}};

    REQUIRE_FALSE(MockAllocatorWithoutPOCAPOMATraits::propagate_on_container_copy_assignment::value);
    REQUIRE_FALSE(MockAllocatorWithoutPOCAPOMATraits::propagate_on_container_move_assignment::value);

    REQUIRE(MockAllocatorWithPOCAPOMATraits::propagate_on_container_copy_assignment::value);
    REQUIRE(MockAllocatorWithPOCAPOMATraits::propagate_on_container_move_assignment::value);

    REQUIRE(assigned_to_with_poca_poma.get_allocator() != lst_diff_allocator_with_poca_poma.get_allocator());
    REQUIRE(assigned_to_without_poca_poma.get_allocator() != lst_diff_allocator_without_poca_poma.get_allocator());

    MockItem::reset();
    SECTION("Copy assignment (same allocator)") {
        assigned_to = assigned_from;

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 4 && assigned_to.back() == 5));
        REQUIRE(assigned_to == assigned_from);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 2);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 1);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("Copy assignment (diff allocator, POCA false)") {
        assigned_to_without_poca_poma = lst_diff_allocator_without_poca_poma;

        REQUIRE(assigned_to_without_poca_poma.size() == 3);
        REQUIRE((assigned_to_without_poca_poma.front() == 4 && assigned_to_without_poca_poma.back() == 5));
        REQUIRE(assigned_to_without_poca_poma == lst_diff_allocator_without_poca_poma);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 2);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 1);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("Copy assignment (diff allocator, POCA true)") {
        assigned_to_with_poca_poma = lst_diff_allocator_with_poca_poma;

        REQUIRE(assigned_to_with_poca_poma.size() == 3);
        REQUIRE((assigned_to_with_poca_poma.front() == 4 && assigned_to_with_poca_poma.back() == 5));
        REQUIRE(assigned_to_with_poca_poma == lst_diff_allocator_with_poca_poma);

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 3);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("Move assignment (same allocator)") {
        assigned_to = std::move(assigned_from);

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 4 && assigned_to.back() == 5));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("Move assignment (diff allocator, POMA false)") {
        assigned_to_without_poca_poma = std::move(lst_diff_allocator_without_poca_poma);

        REQUIRE(assigned_to_without_poca_poma.size() == 3);
        REQUIRE((assigned_to_without_poca_poma.front() == 4 && assigned_to_without_poca_poma.back() == 5));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 2);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 1);
    }
    SECTION("Move assignment (diff allocator, POMA true)") {
        assigned_to_with_poca_poma = std::move(lst_diff_allocator_with_poca_poma);

        REQUIRE(assigned_to_with_poca_poma.size() == 3);
        REQUIRE((assigned_to_with_poca_poma.front() == 4 && assigned_to_with_poca_poma.back() == 5));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_constructed == 0);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 0);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("Initializer list assignment") {
        assigned_to = {4, 10, 5};

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 4 && assigned_to.back() == 5));

        REQUIRE(MockItem::constructed == 3);
        REQUIRE(MockItem::destructed == 3);
        REQUIRE(MockItem::copy_constructed == 2);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 1);
        REQUIRE(MockItem::move_assigned == 0);
    }
}

TEST_CASE("assign") {
    list<MockItem, MockAllocator> assigned_to{1, 2};
    std::vector<MockItem> vec{10, 20, 30};
    MockItem item{10};
    MockItem::reset();
    SECTION("From count & value (larger)") {
        assigned_to.assign(3, item);

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 10 && assigned_to.back() == 10));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 1);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 2);
        REQUIRE(MockItem::move_assigned == 0);
    }
    SECTION("From count & value (smaller)") {
        assigned_to.assign(1, item);

        REQUIRE(assigned_to.size() == 1);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(MockItem::copy_assigned == 1);
    }

    SECTION("From iterator") {
        assigned_to.assign(vec.begin(), vec.end());

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 10 && assigned_to.back() == 30));

        REQUIRE(MockItem::constructed == 0);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE(MockItem::copy_constructed == 1);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 2);
        REQUIRE(MockItem::move_assigned == 0);
    }

    SECTION("From initializer list") {
        assigned_to.assign({4, 10, 5});

        REQUIRE(assigned_to.size() == 3);
        REQUIRE((assigned_to.front() == 4 && assigned_to.back() == 5));

        REQUIRE(MockItem::constructed == 3);
        REQUIRE(MockItem::destructed == 3);
        REQUIRE(MockItem::copy_constructed == 1);
        REQUIRE(MockItem::move_constructed == 0);
        REQUIRE(MockItem::copy_assigned == 2);
        REQUIRE(MockItem::move_assigned == 0);
    }
}

TEST_CASE("Empty") {
    list<MockItem> lst{};
    REQUIRE(lst.empty());
    lst.push_back(1);
    REQUIRE_FALSE(lst.empty());
    lst.pop_back();
    REQUIRE(lst.empty());
}

TEST_CASE("Clear") {
    list<MockItem, MockAllocator> lst{1, 2, 3};
    MockItem::reset();
    lst.clear();
    REQUIRE(lst.empty());
    REQUIRE(lst.size() == 0);
    REQUIRE(MockItem::destructed == 3);
}

TEST_CASE("Insert") {
    list<MockItem> lst{1, 2, 3};
    std::vector<MockItem> vec{10, 20, 30};
    MockItem item{10};
    MockItem::reset();
    SECTION ("Copy insert item"){
        lst.insert(std::next(lst.begin()), item);
        REQUIRE(MockItem::copy_constructed == 1);
        REQUIRE(lst == list<MockItem>{1, 10, 2, 3});
    }
    SECTION ("Move insert item"){
        lst.insert(std::next(lst.begin()), std::move(item));
        REQUIRE(MockItem::move_constructed == 1);
        REQUIRE(lst == list<MockItem>{1, 10, 2, 3});
    }
    SECTION ("Insert pos, count, value"){
        lst.insert(std::next(lst.begin()), 5, item);
        REQUIRE(MockItem::copy_constructed == 5);
        REQUIRE(lst == list<MockItem>{1, 10, 10, 10, 10, 10, 2, 3});
    }
    SECTION ("Insert from iterator"){
        lst.insert(std::next(lst.begin()), vec.begin(), vec.end());
        REQUIRE(MockItem::copy_constructed == 3);
        REQUIRE(lst == list<MockItem>{1, 10, 20, 30, 2, 3});
    }
    SECTION ("Insert from initializer list"){
        lst.insert(std::next(lst.begin()), {10, 20, 30});
        REQUIRE(MockItem::copy_constructed == 3);
        REQUIRE(lst == list<MockItem>{1, 10, 20, 30, 2, 3});
    }
}

TEST_CASE("Emplace") {
    list<MockItem> lst{1, 2, 3};
    MockItem::reset();
    lst.emplace(lst.begin(), 10);
    REQUIRE(MockItem::constructed == 1);
    REQUIRE(lst.size() == 4);
    REQUIRE(lst == list<MockItem>{10, 1, 2, 3});
}

TEST_CASE("Push front / back") {
    list<MockItem> lst{1, 2, 3};
    MockItem item{10};

    MockItem::reset();
    lst.push_front(item);
    REQUIRE(MockItem::copy_constructed == 1);
    REQUIRE(lst.size() == 4);
    REQUIRE((lst.front() == 10 && lst.back() == 3));

    MockItem::reset();
    lst.push_back(item);
    REQUIRE(MockItem::copy_constructed == 1);
    REQUIRE(lst.size() == 5);
    REQUIRE((lst.front() == 10 && lst.back() == 10));

    MockItem::reset();
    lst.push_back(MockItem{5});
    REQUIRE(MockItem::copy_constructed == 0);
    REQUIRE(MockItem::move_constructed == 1);
    REQUIRE(lst.size() == 6);
    REQUIRE((lst.front() == 10 && lst.back() == 5));
}

TEST_CASE("Emplace front / back") {
    list<MockItem> lst{1, 2, 3};

    MockItem::reset();
    lst.emplace_front(10);
    REQUIRE(MockItem::constructed == 1);
    REQUIRE(lst.size() == 4);
    REQUIRE((lst.front() == 10 && lst.back() == 3));

    MockItem::reset();
    lst.emplace_back(10);
    REQUIRE(MockItem::constructed == 1);
    REQUIRE(lst.size() == 5);
    REQUIRE((lst.front() == 10 && lst.back() == 10));
}

TEST_CASE("Pop front / back") {
    list<MockItem> lst{1, 2, 3};

    MockItem::reset();
    lst.pop_front();
    REQUIRE(MockItem::destructed == 1);
    REQUIRE(lst.size() == 2);
    REQUIRE((lst.front() == 2 && lst.back() == 3));

    MockItem::reset();
    lst.pop_back();
    REQUIRE(MockItem::destructed == 1);
    REQUIRE(lst.size() == 1);
    REQUIRE((lst.front() == 2 && lst.back() == 2));
}

TEST_CASE("Erase") {
    list<MockItem> lst{1, 2, 3, 4, 5};

    MockItem::reset();
    lst.erase(std::next(lst.begin(), 1));
    REQUIRE(MockItem::destructed == 1);
    REQUIRE(lst == list<MockItem>{1, 3, 4, 5});

    MockItem::reset();
    lst.erase(std::next(lst.begin(), 2), lst.end());
    REQUIRE(MockItem::destructed == 2);
    REQUIRE(lst == list<MockItem>{1, 3});
}
////
////TEST_CASE("Resize") {
////    list<MockItem> lst{1, 2, 3, 4, 5};
////
////    MockItem::reset();
////    lst.resize(3);
////    REQUIRE(MockItem::destructed == 2);
////    REQUIRE(lst == list<MockItem>{1, 2, 3});
////
////    MockItem::reset();
////    lst.resize(5);
////    REQUIRE(MockItem::constructed == 2);
////    REQUIRE(lst == list<MockItem>{1, 2, 3, 0, 0});
////
////    MockItem::reset();
////    lst.resize(6, MockItem{10});
////    REQUIRE(MockItem::copy_constructed == 1);
////    REQUIRE(lst == list<MockItem>{1, 2, 3, 0, 0, 10});
////}
////
////TEST_CASE("Swap") {
////    SECTION("Same allocator") {
////        list<MockItem> lst{1, 2, 3, 4, 5};
////        list<MockItem> lst2{10, 20};
////
////        MockItem::reset();
////        lst.swap(lst2);
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst2 == list<MockItem>{1, 2, 3, 4, 5});
////        REQUIRE(lst == list<MockItem>{10, 20});
////    }
////    SECTION("Diff allocator with propagate on swap") {
////        list<MockItem, MockAllocatorWithPOS> lst{{1, 2, 3, 4, 5}, MockAllocatorWithPOS{1}};
////        list<MockItem, MockAllocatorWithPOS> lst2{{10, 20}, MockAllocatorWithPOS{2}};
////        REQUIRE(lst.get_allocator().id == 1);
////        REQUIRE(lst2.get_allocator().id == 2);
////
////        lst.swap(lst2);
////        REQUIRE(lst.get_allocator().id == 2);
////        REQUIRE(lst2.get_allocator().id == 1);
////    }
////}
////
////TEST_CASE("Merge") {
////    list<MockItem> lst{1, 2, 30, 40, 41};
////    list<MockItem> lst2{{3, 4, 35, 45}, lst.get_allocator()};
////    MockItem::reset();
////    SECTION("lvalue reference merge") {
////        lst.merge(lst2);
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{1, 2, 3, 4, 30, 35, 40, 41, 45});
////        REQUIRE(lst2.empty());
////    }
////    SECTION("rvalue reference merge") {
////        lst.merge(std::move(lst2));
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{1, 2, 3, 4, 30, 35, 40, 41, 45});
////        REQUIRE(lst2.empty());
////    }
////    SECTION("lvalue reference merge with custom comparitor") {
////        auto backwards_order = [](const MockItem& lhs, const MockItem& rhs) {
////            return rhs < lhs;
////        };
////
////        lst.reverse();
////        lst2.reverse();
////        lst.merge(lst2, backwards_order);
////
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst2.empty());
////        REQUIRE(lst == list<MockItem>{45, 41, 40, 35, 30, 4, 3, 2, 1});
////    }
////}
////
////TEST_CASE("Splice") {
////    list<MockItem> lst{1, 2, 3, 4, 5};
////    list<MockItem> lst2{{10, 20, 30}, lst.get_allocator()};
////    MockItem::reset();
////    SECTION("All items") {
////        lst.splice(lst.begin(), lst2);
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{10, 20, 30, 1, 2, 3, 4, 5});
////        REQUIRE(lst2.empty());
////    }
////    SECTION("Single item") {
////        lst.splice(lst.begin(), lst2, lst2.begin());
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{10, 1, 2, 3, 4, 5});
////        REQUIRE(lst2 == list<MockItem>{20, 30});
////    }
////    SECTION("Item range") {
////        lst.splice(std::next(lst.begin()), lst2, lst2.begin(), std::next(lst2.begin(), 2));
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{1, 10, 20, 2, 3, 4, 5});
////        REQUIRE(lst2.size() == 1);
////    }
////}
////
////TEST_CASE("Remove / RemoveIf") {
////    list<MockItem> lst{1, 2, 3, 1, 2, 3};
////    MockItem item{1};
////    MockItem::reset();
////    SECTION("Remove") {
////        lst.remove(item);
////        REQUIRE(MockItem::destructed == 2);
////        REQUIRE(lst == list<MockItem>{2, 3, 2, 3});
////    }
////    SECTION("Remove If") {
////        auto is_even = [](const MockItem& item) {
////            return item.value % 2 == 0;
////        };
////        lst.remove_if(is_even);
////        REQUIRE(MockItem::destructed == 2);
////        REQUIRE(lst == list<MockItem>{1, 3, 1, 3});
////    }
////}
////
////TEST_CASE("Reverse") {
////    list<MockItem> lst{1, 2, 3};
////    MockItem::reset();
////    lst.reverse();
////    REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////    REQUIRE(lst == list<MockItem>{3, 2, 1});
////}
////
////TEST_CASE("Unique") {
////    list<MockItem> lst{1, 2, 2, 3, 4, 4};
////    MockItem::reset();
////    SECTION("Same value") {
////        auto count = lst.unique();
////        REQUIRE(count == 2);
////        REQUIRE(MockItem::destructed == 2);
////        REQUIRE(lst == list<MockItem>{1, 2, 3, 4});
////    }
////    SECTION("With Binary predicate") {
////        auto sum_is_odd = [](const MockItem& left, const MockItem& right) {
////            return (left.value + right.value) % 2 == 1;
////        };
////        auto count = lst.unique(sum_is_odd);
////        REQUIRE(count == 4);
////        REQUIRE(MockItem::destructed == 4);
////        REQUIRE(lst == list<MockItem>{1, 3});
////    }
////}
////
////TEST_CASE("Sort") {
////    list<MockItem> lst{4, 7, 1, 5};
////    MockItem::reset();
////    SECTION("Sort") {
////        lst.sort();
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{1, 4, 5, 7});
////    }
////    SECTION("Sort with custom comparison ") {
////        auto four_first = [](const MockItem& left, const MockItem& right) {
////            return (left == 4) || (right != 4 && (left.value < right.value));
////        };
////        lst.sort(four_first);
////        REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
////        REQUIRE(lst == list<MockItem>{4, 1, 5, 7});
////    }
////}