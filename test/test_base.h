#ifndef TEST_BASE_H
#define TEST_BASE_H

#include <new>

static int CALLS_TO_NEW = 0;
static int CALLS_TO_DELETE = 0;

void* operator new (std::size_t sz) {
    CALLS_TO_NEW++;
    if (sz == 0)
        ++sz; // avoid std::malloc(0) which may return nullptr on success

    if (void *ptr = std::malloc(sz))
        return ptr;

    throw std::bad_alloc{};
}

void operator delete (void* ptr) noexcept {
    CALLS_TO_DELETE++;
    std::free(ptr);
}


struct MockItem {
    MockItem(int value = 0) : value{value} {
        constructed++;
    };

    ~MockItem() {
        destructed++;
    }

    MockItem(const MockItem& other) : value(other.value) {
        copy_constructed++;
    }

    MockItem(MockItem&& other) noexcept : value(other.value) {
        move_constructed++;
    }

    MockItem& operator=(const MockItem& other) {
        value = other.value;
        copy_assigned++;
        return *this;
    }

    MockItem& operator=(MockItem&& other) noexcept {
        value = other.value;
        move_assigned++;
        return *this;
    }

    static int constructed_assigned_or_destroyed() {
        return constructed + destructed + copy_constructed + copy_assigned + move_constructed + move_assigned;
    }

    static void reset() {
        constructed = 0;
        destructed = 0;
        copy_constructed = 0;
        copy_assigned = 0;
        move_constructed = 0;
        move_assigned = 0;
    }

    bool operator==(int r) const {
        return value == r;
    }

    bool operator==(const MockItem& r) const {
        return value == r.value;
    }

    bool operator<(const MockItem& r) const {
        return value < r.value;
    }

    int value;
    static int constructed;
    static int destructed;
    static int copy_constructed;
    static int copy_assigned;
    static int move_constructed;
    static int move_assigned;
};

int MockItem::constructed = 0;
int MockItem::destructed = 0;
int MockItem::copy_constructed = 0;
int MockItem::copy_assigned = 0;
int MockItem::move_constructed = 0;
int MockItem::move_assigned = 0;

template<class T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& os, const MockItem& x) {
    return os << x.value;
}

template<typename T, typename POCA, typename POMA, typename POS = std::false_type>
class MockAlloc : public std::allocator<T> {
public:
    using propagate_on_container_copy_assignment = POCA;
    using propagate_on_container_move_assignment = POMA;
    using propagate_on_container_swap = POS;
    using is_always_equal = std::false_type;

    MockAlloc(int id = 0)
            : std::allocator<T>{}, id{id} {};

    MockAlloc(const MockAlloc& other)
            : std::allocator<T>{}, id{other.id} {};

    MockAlloc(MockAlloc&& other) noexcept
            : std::allocator<T>{}, id{other.id} {};

    MockAlloc& operator=(const MockAlloc& other) noexcept {
        if(*this == other) return *this;
        id = other.id;
        return *this;
    };

    template<typename U, typename U_POCA, typename U_POMA, typename U_POS>
    MockAlloc(const MockAlloc<U, U_POCA, U_POMA, U_POS>& other)
            : std::allocator<T>{}, id{other.id} {}

    template<typename U, typename U_POCA, typename U_POMA, typename U_POS>
    MockAlloc(MockAlloc<U, U_POCA, U_POMA, U_POS>&& other)
            : std::allocator<T>{}, id{other.id} {}

    bool operator==(const MockAlloc& rhs) {
        return id == rhs.id;
    }

    int id;
};

using MockAllocator = MockAlloc<MockItem, std::false_type, std::true_type>;
using MockAllocatorTraits = std::allocator_traits<MockAllocator>;

using MockAllocatorWithPOCAPOMA = MockAlloc<MockItem, std::true_type, std::true_type>;
using MockAllocatorWithPOCAPOMATraits = std::allocator_traits<MockAllocatorWithPOCAPOMA>;

using MockAllocatorWithoutPOCAPOMA = MockAlloc<MockItem, std::false_type, std::false_type>;
using MockAllocatorWithoutPOCAPOMATraits = std::allocator_traits<MockAllocatorWithoutPOCAPOMA>;

using MockAllocatorWithPOS = MockAlloc<MockItem, std::true_type, std::true_type, std::true_type>;
using MockAllocatorWithPOSTraits = std::allocator_traits<MockAllocatorWithPOS>;

void reset() {
    MockItem::reset();
    CALLS_TO_NEW = 0;
    CALLS_TO_DELETE = 0;
}

#endif // TEST_BASE_H