#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include <test/test_base.h>
#include <memory/allocator.h>

#include <vector>
#include <list>

using minimal_allocator_type = minimal_allocator<MockItem>;
using arena_allocator_type = arena_allocator<MockItem>;

TEST_CASE("Calls to new / delete") {
    reset();
    REQUIRE((CALLS_TO_NEW == 0 && CALLS_TO_DELETE == 0));
    int* a = new int{1};
    REQUIRE(CALLS_TO_NEW == 1);
    delete a;
    REQUIRE(CALLS_TO_DELETE == 1);
}

TEST_CASE("Default allocator") {
    reset();
    std::allocator<MockItem> alloc{};
    MockItem* ptr = alloc.allocate(5);
    alloc.deallocate(ptr, 5);
    REQUIRE(MockItem::constructed == 0);
    REQUIRE(MockItem::destructed == 0);
}

TEST_CASE("Minimal allocator") {
    minimal_allocator_type alloc{};
    SECTION("Default constructor") {
        reset(); // Need to put this within the section, must be the Catch framework is calling new
        auto ptr = alloc.allocate(5);
        REQUIRE((CALLS_TO_NEW == 1 && CALLS_TO_DELETE == 0));
        REQUIRE(MockItem::constructed == 0);
        for (int i = 0; i < 5; i++) {
            std::construct_at(ptr + i);
        }
        REQUIRE(MockItem::constructed == 5);
        alloc.deallocate(ptr, 5);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE((CALLS_TO_NEW == 1 && CALLS_TO_DELETE == 1));
    }
    SECTION("Converting constructor") {
        reset();
        minimal_allocator<int> other{alloc};
        auto ptr = other.allocate(5);
        REQUIRE((CALLS_TO_NEW == 1 && CALLS_TO_DELETE == 0));
        for(int i = 0; i < 5; i++) {
            std::construct_at(ptr + i);
        }
        other.deallocate(ptr, 5);
        REQUIRE((CALLS_TO_NEW == 1 && CALLS_TO_DELETE == 1));
    }
}

TEST_CASE("Arena") {
    Arena<100> arena{};
    auto start = arena.alloc(10);
    auto p_offset = [start](void* ptr) -> long {
        return static_cast<unsigned char*>(ptr) - static_cast<unsigned char*>(start);
    };

    auto dealloc = [&, start](long position, long size) -> void {
        arena.dealloc(static_cast<void*>(static_cast<unsigned char*>(start) + position), size);
    };

    SECTION("Attempt to alloc too large a region") {
        REQUIRE_THROWS(arena.alloc(101));
    }
    SECTION("Successful allocations from left") {
        REQUIRE(p_offset(arena.alloc(10)) == 10);
        REQUIRE(p_offset(arena.alloc(10)) == 20);
    }
    SECTION("Successful dealloc") {
        REQUIRE(p_offset(arena.alloc(10)) == 10);
        REQUIRE(p_offset(arena.alloc(10)) == 20);
        dealloc(20, 10);
        REQUIRE(p_offset(arena.alloc(10)) == 20);
    }
    SECTION("Allocate in first available gap") {
        REQUIRE(p_offset(arena.alloc(50)) == 10);
        REQUIRE(p_offset(arena.alloc(10)) == 60);
        REQUIRE(p_offset(arena.alloc(5)) == 70);
        REQUIRE(p_offset(arena.alloc(25)) == 75);
        dealloc(10, 50);
        dealloc(70, 5);
        REQUIRE(p_offset(arena.alloc(10)) == 10);
        REQUIRE(p_offset(arena.alloc(3)) == 20);
    }
    SECTION("Combine gaps to make space for large item") {
        REQUIRE(p_offset(arena.alloc(50)) == 10);
        REQUIRE(p_offset(arena.alloc(10)) == 60);
        REQUIRE(p_offset(arena.alloc(5)) == 70);
        REQUIRE(p_offset(arena.alloc(25)) == 75);
        dealloc(10, 50);
        dealloc(70, 5);
        REQUIRE_THROWS(arena.alloc(62));
        dealloc(60, 10);
        REQUIRE(p_offset(arena.alloc(62)) == 10);
    }
}

TEST_CASE("Arena allocator") {
    SECTION("Allocate memory on creation") {
        reset();
        arena_allocator_type alloc{};
        REQUIRE(CALLS_TO_NEW == 1);
    }
    SECTION("Free memory on destruction") {
        {
            arena_allocator_type alloc{};
            reset();
        }
        REQUIRE(CALLS_TO_DELETE == 1);
    }
    SECTION("No dynamic allocations after construction") {
        arena_allocator_type alloc{};
        reset();
        auto ptr = alloc.allocate(5);
        REQUIRE(MockItem::constructed == 0);
        for (int i = 0; i < 5; i++) {
            std::construct_at(ptr + i);
        }
        REQUIRE(MockItem::constructed == 5);
        alloc.deallocate(ptr, 5);
        REQUIRE(MockItem::destructed == 0);
        REQUIRE((CALLS_TO_NEW == 0 && CALLS_TO_DELETE == 0));
    }
    SECTION("Throw exception when full") {
        arena_allocator_type alloc{};
        alloc.allocate(100 / sizeof(MockItem));
        REQUIRE_THROWS(alloc.allocate(1));
    }
    SECTION("Only one dynamic allocation when passed to list") {
        reset();
        std::list<MockItem> lst{};
        lst.emplace_back(1);
        lst.emplace_back(2);
        lst.emplace_back(3);
        REQUIRE(CALLS_TO_NEW == 3);

        reset();
        arena_allocator_type alloc{};
        std::list<MockItem, decltype(alloc)> lst_with_arena_alloc{alloc};
        lst_with_arena_alloc.emplace_back(1);
        lst_with_arena_alloc.emplace_back(2);
        lst_with_arena_alloc.emplace_back(3);
        REQUIRE(CALLS_TO_NEW == 1);
    }
}