#include <catch2/catch_test_macros.hpp>

#include <iostream>
#include <memory/smart_pointers.h>
#include <test/test_base.h>

//template<class T, class Deleter = std::default_delete<T>>
//using unique_ptr = std::unique_ptr<T, Deleter>;


TEST_CASE("Constructor") {
    MockItem::reset();
    auto ptr = new MockItem();
    SECTION ("Default") {
        auto up = unique_ptr<MockItem>{};
        REQUIRE(up.get() == nullptr);
        REQUIRE_FALSE(up);
    }
    SECTION ("From object ptr") {
        auto up = unique_ptr<MockItem>{ptr};
        REQUIRE(up.get() == ptr);
        REQUIRE(up);
    }
    SECTION ("Move constructor") {
        auto moved_from_up = unique_ptr<MockItem>{ptr};
        auto moved_into_up = unique_ptr<MockItem>{std::move(moved_from_up)};
        REQUIRE(moved_into_up.get() == ptr);
        REQUIRE(moved_into_up);
    }
}

TEST_CASE("operator=") {
    SECTION ("Assigned nullptr") {
        auto up = unique_ptr<MockItem>{new MockItem()};
        MockItem::reset();
        up = nullptr;
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(up.get() == nullptr);
        REQUIRE_FALSE(up);
    }
    SECTION ("Move assignment") {
        auto up1 = unique_ptr<MockItem>{new MockItem()};
        auto ptr = new MockItem();

        MockItem::reset();
        up1 = unique_ptr<MockItem>{ptr};
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(up1.get() == ptr);
    }
}

TEST_CASE("Destructor") {
    MockItem::reset();
    auto ptr = new MockItem();
    {
        auto up = unique_ptr<MockItem>{ptr};
        REQUIRE(MockItem::destructed == 0);
    }
    REQUIRE(MockItem::destructed == 1);
}

TEST_CASE("Release") {
    MockItem::reset();
    auto ptr = new MockItem();
    {
        auto up = unique_ptr<MockItem>{ptr};
        REQUIRE(up.release() == ptr);
    }
    REQUIRE(MockItem::destructed == 0);
}


TEST_CASE("Reset") {
    MockItem::reset();
    auto ptr = new MockItem();
    auto other_ptr = new MockItem();
    {
        auto up = unique_ptr<MockItem>{ptr};
        up.reset(other_ptr);
        REQUIRE(MockItem::destructed == 1);
        REQUIRE(up.get() == other_ptr);
    }
    REQUIRE(MockItem::destructed == 2);
}

TEST_CASE("Swap") {
    auto ptr = new MockItem();
    auto other_ptr = new MockItem();
    auto up = unique_ptr<MockItem>{ptr};
    auto other_up = unique_ptr<MockItem>{other_ptr};

    MockItem::reset();
    up.swap(other_up);

    REQUIRE(MockItem::constructed_assigned_or_destroyed() == 0);
    REQUIRE(up.get() == other_ptr);
    REQUIRE(other_up.get() == ptr);
}

TEST_CASE("operator* / operator->") {
    auto up = unique_ptr<MockItem>{new MockItem()};
    REQUIRE((*up).value == 0);
    REQUIRE(up->value == 0);
}

// Helper class for runtime polymorphism test
int B_bar = 0;
int D_bar = 0;
int D_constructor = 0;
int D_destructor = 0;

struct B
{
    virtual ~B() = default;
    virtual void bar() { B_bar++; }
};

struct D : B
{
    D() { D_constructor++; }
    ~D() { D_destructor++; }

    void bar() override { D_bar++; }
};

void reset() {
    B_bar = 0;
    D_bar = 0;
    D_constructor = 0;
    D_destructor = 0;
    MockItem::reset();
}

// A function consuming a unique_ptr can take it by value or by rvalue reference
unique_ptr<D> pass_through(unique_ptr<D> p)
{
    p->bar();
    return p;
}

TEST_CASE("Unique ownership semantics") {
    reset();
    auto ptr = new D();
    auto p = unique_ptr<D>{ptr};
    unique_ptr<D> q = pass_through(std::move(p));
    REQUIRE(D_constructor == 1);
    REQUIRE(D_destructor == 0);
    REQUIRE(D_bar == 1);
    REQUIRE(q.get() == ptr);
    REQUIRE(!p);
}

TEST_CASE("Runtime polymorphism") {
    reset();
    unique_ptr<B> p = make_unique<D>();
    p->bar();
    REQUIRE(D_bar == 1);
}

TEST_CASE("Custom deleter") {
    reset();
    bool deleted{};
    {
        auto fn = [&](D *ptr) {
            deleted = true;
            delete ptr;
        };
        unique_ptr<D, decltype(fn)> p(new D{}, fn);
    }
    REQUIRE(deleted);
    REQUIRE(D_destructor == 1);
}

TEST_CASE("Arguments are forwarded with make_unique") {
    reset();
    auto ptr = make_unique<std::tuple<MockItem>>(MockItem{});
    REQUIRE(MockItem::constructed == 1);
    REQUIRE(MockItem::move_constructed == 1);
}

