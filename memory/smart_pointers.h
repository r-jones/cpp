#ifndef SMART_POINTERS_H
#define SMART_POINTERS_H

#include <memory>

template<class T, class Deleter = std::default_delete<T>>
class unique_ptr {

public:
    using pointer = T*; // OR std::remove_reference<Deleter>::type::pointer if exists?
    using element_type = T;
    using deleter_type = Deleter;

    // TODO: These overloads participate in overload resolution only if std::is_default_constructible<Deleter>::value
    // is true and Deleter is not a pointer type.
    constexpr unique_ptr() noexcept = default;

    explicit unique_ptr(T* const p) noexcept : ptr{p} {}

    // TODO: see https://en.cppreference.com/w/cpp/memory/unique_ptr/unique_ptr
    unique_ptr(T* const p, Deleter d1) noexcept : ptr{p}, deleter{std::move(d1)} {}
    // unique_ptr(pointer p, /* see below */ d2) noexcept;

    unique_ptr(const unique_ptr& u) = delete;

    // TODO:
    unique_ptr(unique_ptr&& u) noexcept : ptr{u.release()}, deleter{std::move(u.deleter)} {
        static_assert(std::is_move_constructible_v<Deleter>);
    }

    template<class U, class E>
    unique_ptr(const unique_ptr<U, E>& u) = delete;

    template<std::derived_from<T> U, class E>
    unique_ptr(unique_ptr<U, E>&& u) noexcept : ptr{u.release()} {
        static_assert(std::is_convertible<typename unique_ptr<U, E>::pointer, pointer>::value);
        if constexpr(std::is_reference_v<E>) {
            static_assert(std::is_same_v<Deleter, E>);
            deleter = u.deleter;
        } else {
            static_assert(std::is_convertible_v<E, Deleter>);
            deleter = std::move(u.get_deleter());
        }
    }

    ~unique_ptr() noexcept (noexcept(deleter(ptr))) {
        deleter(ptr);
    }

    unique_ptr& operator=(unique_ptr&& r) noexcept {
        if(*this == r) return *this;
        reset(r.release());
        deleter = r.deleter;
        return *this;
    }

    template<std::derived_from<T> U, class E>
    unique_ptr& operator=(unique_ptr<U, E>&& r) noexcept {
        static_assert(std::is_convertible<typename unique_ptr<U, E>::pointer, pointer>::value);
        static_assert(std::is_assignable<Deleter&, E&&>::value);
        reset(r.release());
        deleter = r.deleter;
        return *this;
    }

    unique_ptr& operator=(std::nullptr_t) noexcept {
        reset();
        return *this;
    }

    // Modifiers

    pointer release() noexcept {
        return std::exchange(ptr, nullptr);
    }

    void reset(pointer new_ptr = pointer()) noexcept {
        pointer old_ptr = ptr;
        ptr = new_ptr;
        deleter(old_ptr);
    }

    void swap(unique_ptr& other) noexcept {
        std::swap(ptr, other.ptr);
        std::swap(deleter, other.deleter);
    }

    // Observers

    pointer get() const noexcept {
        return ptr;
    }

    Deleter& get_deleter() noexcept {
        return deleter;
    }

    const Deleter& get_deleter() const noexcept {
        return deleter;
    }

    explicit operator bool() const noexcept {
        return ptr != nullptr;
    }

    typename std::add_lvalue_reference<T>::type operator*() const
    noexcept(noexcept(*std::declval<pointer>())) {
        return *ptr;
    }

    pointer operator->() const noexcept {
        return ptr;
    }

private:
    T* ptr{};
    [[no_unique_address]] Deleter deleter{};
};

template<class T, class... Args>
unique_ptr<T> make_unique(Args&&... args) {
    return unique_ptr<T>{new T{std::forward<Args>(args)...}};
}

// TODO: This overload participates in overload resolution only if os << p.get() is a valid expression.
template <class CharT, class Traits, class Y, class D>
std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, const unique_ptr<Y, D>& p) {
    return os << p.get();
}

template<class T, class D>
bool operator==(const unique_ptr<T, D>& x, const unique_ptr<T, D>& y) noexcept {
    return x.get() == y.get();
}

template<class T>
class weak_ptr;

struct _shared_counts {
    size_t reference_count;
    size_t weak_reference_count;
};

template<class T>
class shared_ptr {

public:

    using element_type = T;
    using weak_type = weak_ptr<T>;

    constexpr shared_ptr() noexcept = default;

    template<class Y>
    explicit shared_ptr(Y* ptr)
        : counter{new _shared_counts{1, 1}}, pointer{ptr} {
        static_assert(std::is_convertible<Y*, T*>::value);
    }

    // TODO:
    //    template<class Y, class Deleter,
    //            std::enable_if<std::is_invocable_v<Deleter, Y>, bool>::type = true>
    //    shared_ptr(Y* ptr, Deleter d);
    //    template<class Deleter,
    //            std::enable_if<std::is_invocable_v<Deleter, std::nullptr_t>, bool>::type = true>
    //    shared_ptr(std::nullptr_t ptr, Deleter d);

    shared_ptr(const shared_ptr& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        if(counter) counter->reference_count++;
    }

    template<class Y>
    shared_ptr(const shared_ptr<Y>& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        if(counter) counter->reference_count++;
    }

    shared_ptr(shared_ptr&& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        r.counter = nullptr;
        r.pointer = nullptr;
    }

    template<class Y>
    shared_ptr(shared_ptr<Y>&& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        r.counter = nullptr;
        r.pointer = nullptr;
    }

    template<class Y>
    shared_ptr(const shared_ptr<Y>& r, T* ptr) noexcept
        : shared_ptr(r) {
            aliasing_pointer = ptr;
            static_assert(std::is_convertible<Y*, T*>::value);
        };

    template<class Y>
    shared_ptr(shared_ptr<Y>&& r, T* ptr) noexcept
        : shared_ptr(std::move(r)) {
            aliasing_pointer = ptr;
            static_assert(std::is_convertible<Y*, T*>::value);
        };

    template<class Y>
    explicit shared_ptr(const weak_ptr<Y>& r) : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        if(r.expired()) {
            throw std::bad_weak_ptr{};
        }
        counter->reference_count++;
    }

    template<class Y, class Deleter>
    shared_ptr(unique_ptr<Y, Deleter>&& r) : shared_ptr(r.release()) {
        static_assert(std::is_convertible<typename std::unique_ptr<Y, Deleter>::pointer, T*>::value);
        if constexpr (std::is_reference_v<Deleter>) {
            deleter = r.get_deleter();
        } else {
            deleter = std::move(r.get_deleter());
        }
    }

    ~shared_ptr() {
        reset();
    }

    shared_ptr& operator=(const shared_ptr& r) noexcept {
        if(*this == r) return *this;
        reset();
        counter = r.counter;
        pointer = r.pointer;
        if(counter) {
            counter->reference_count++;
        }
        return *this;
    }

    template<class Y>
    shared_ptr& operator=(const shared_ptr<Y>& r) noexcept {
        static_assert(std::is_convertible<Y*, T*>::value);
        if(*this == r) return *this;
        reset();
        counter = r.counter;
        pointer = r.pointer;
        if(counter) {
            counter->reference_count++;
        }
        return *this;
    }

    shared_ptr& operator=(shared_ptr&& r) noexcept {
        if(*this == r) return *this;
        reset();
        counter = r.counter;
        pointer = r.pointer;
        r.counter = nullptr;
        r.pointer = nullptr;
        return *this;
    }

    // TODO: can replace this and above with shared_ptr<T>(std::move(r)).swap(*this) or similar
    // Is that better?
    template<class Y>
    shared_ptr& operator=(shared_ptr<Y>&& r) noexcept {
        static_assert(std::is_convertible<Y*, T*>::value);
        if(*this == r) return *this;
        reset();
        counter = r.counter;
        pointer = r.pointer;
        r.counter = nullptr;
        r.pointer = nullptr;
        return *this;
    }

    template<class Y, class Deleter>
    shared_ptr& operator=(std::unique_ptr<Y,Deleter>&& r) {
        shared_ptr<T>(r).swap(*this);
        return this;
    }

    // Modifiers

    void reset() noexcept {
        if(counter) {
            counter->reference_count--;
            if (counter->reference_count == 0) {
                deleter(pointer);
                if (counter->weak_reference_count == 0) {
                    delete counter;
                }
            }
        }
        counter = nullptr;
        pointer = nullptr;
    }

    template<class Y>
    void reset(Y* ptr) {
        static_assert(std::is_convertible<Y*, T*>::value);
        shared_ptr<T>(ptr).swap(*this);
    }

    // TODO: Y must be a complete type and implicitly convertible to T.
    template<class Y, class Deleter>
    void reset(Y* ptr, Deleter d) {
        static_assert(std::is_convertible<Y*, T*>::value);
        shared_ptr<T>(ptr, d).swap(*this);
    }

    void swap(shared_ptr& r) noexcept {
        std::swap(counter, r.counter);
        std::swap(pointer, r.pointer);
        std::swap(deleter, r.deleter);
        std::swap(aliasing_pointer, r.aliasing_pointer);
    }

    // Observers

    element_type* get() const noexcept {
        return (aliasing_pointer == nullptr) ? pointer : aliasing_pointer;
    }

    T& operator*() const noexcept {
        return *get();
    }

    T* operator->() const noexcept {
        return get();
    }

    long use_count() const noexcept {
        if(!counter) return 0;
        return counter->reference_count;
    }

    explicit operator bool() const noexcept {
        return get() != nullptr;
    }

    template<class Y>
    bool owner_before(const shared_ptr<Y>& other) const noexcept {
        return counter < other.counter;
    }

    template<class Y>
    bool owner_before(const std::weak_ptr<Y>& other) const noexcept {
        return counter < other.counter;
    }

private:
    _shared_counts* counter{};
    element_type* pointer{};
    element_type* aliasing_pointer{};
    std::default_delete<T> deleter{};

    template<class Y>
    friend class weak_ptr;

    template<class Y, class... Args>
    friend shared_ptr<Y> make_shared(Args&&... args);
};

template<class T>
struct _object_with_shared_counts : public _shared_counts {
    template<class... Args>
    _object_with_shared_counts(Args... args) : obj{std::forward<Args>(args)...} {};
    T obj;
};

template<class T, class... Args>
shared_ptr<T> make_shared(Args&&... args) {
    shared_ptr<T> m_shared_ptr{};
    auto m_object_with_shared_counts = new _object_with_shared_counts<T>{args...};
    m_shared_ptr.counter = m_object_with_shared_counts;
    m_shared_ptr.pointer = &(m_object_with_shared_counts->obj);
    return m_shared_ptr;
}

template <class T, class U>
bool operator==(const shared_ptr<T>& lhs, const shared_ptr<U>& rhs) noexcept {
    return lhs.get() == rhs.get();
}

template<class T, class U, class V>
std::basic_ostream<U, V>& operator<<(std::basic_ostream<U, V>& os, const shared_ptr<T>& ptr) {
    return os << ptr.get();
}

template<class T>
class weak_ptr {
public:
    using element_type = T;

    constexpr weak_ptr() noexcept = default;

    weak_ptr(const weak_ptr& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        if(counter) {
            counter->weak_reference_count++;
        }
    }

    template<class Y>
    weak_ptr(const weak_ptr<Y>& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        if(counter) {
            counter->weak_reference_count++;
        }
    }

    template<class Y>
    weak_ptr(const shared_ptr<Y>& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        counter->weak_reference_count++;
    }

    weak_ptr(weak_ptr&& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        r.counter = nullptr;
        r.pointer = nullptr;
    }

    template< class Y >
    weak_ptr(weak_ptr<Y>&& r) noexcept : counter{r.counter}, pointer{r.pointer} {
        static_assert(std::is_convertible<Y*, T*>::value);
        r.counter = nullptr;
        r.pointer = nullptr;
    }

    ~weak_ptr() {
        reset();
    }

    weak_ptr& operator=(const weak_ptr& r) noexcept {
        if (*this == r) return *this;
        weak_ptr<T>(r).swap(*this);
        return *this;
    }

    template<class Y>
    weak_ptr& operator=(const weak_ptr<Y>& r) noexcept {
        if (*this == r) return *this;
        weak_ptr<T>(r).swap(*this);
        return *this;
    }

    template<class Y>
    weak_ptr& operator=(const shared_ptr<Y>& r) noexcept {
        if (*this == r) return *this;
        weak_ptr<T>(r).swap(*this);
        return *this;
    }

    weak_ptr& operator=(weak_ptr&& r) noexcept {
        if (*this == r) return *this;
        weak_ptr<T>(std::move(r)).swap(*this);
        return *this;
    }

    template<class Y>
    weak_ptr& operator=(weak_ptr<Y>&& r)  noexcept {
        if (*this == r) return *this;
        weak_ptr<T>(std::move(r)).swap(*this);
        return *this;
    }

    // Modifiers

    void reset() noexcept {
        _reduce_weak_ref_count();
        counter = nullptr;
        pointer = nullptr;
    }

    void swap(weak_ptr& r) noexcept {
        std::swap(counter, r.counter);
        std::swap(pointer, r.pointer);
    }

    // Observers

    [[nodiscard]] long use_count() const noexcept {
        return counter ? counter->reference_count : 0;
    }

    [[nodiscard]] bool expired() const noexcept {
        return use_count() == 0;
    }

    shared_ptr<T> lock() const noexcept {
        return expired() ? shared_ptr<T>() : shared_ptr<T>(*this);
    }

    template<class Y>
    bool owner_before(const shared_ptr<Y>& other) const noexcept {
        return counter < other.counter;
    }

    template<class Y>
    bool owner_before(const std::weak_ptr<Y>& other) const noexcept {
        return counter < other.counter;
    }

private:
    void _reduce_weak_ref_count() {
        if(counter) {
            counter->weak_reference_count--;
            if(counter->reference_count == 0 && counter->weak_reference_count == 0) {
                delete counter;
                counter = nullptr;
            }
        }
    }

    bool operator==(const weak_ptr& rhs) {
        return counter == rhs.counter && pointer == rhs.pointer;
    }

    _shared_counts* counter{};
    element_type* pointer{};

    template<class Y>
    friend class shared_ptr;
};

#endif // SMART_POINTERS_H