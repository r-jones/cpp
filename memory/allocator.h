#include <type_traits>
#include <memory>
#include <set>

struct alloc_failed : public std::exception {
    alloc_failed(std::string error) : error{std::move(error)} {}
    [[nodiscard]] const char* what() const noexcept {
        return error.c_str();
    }
    std::string error;
};

template<class T>
class minimal_allocator {
public:
    using value_type = T;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    using propagate_on_container_move_assignment = std::true_type;
    using is_always_equal = std::true_type;

    minimal_allocator() = default;

    template<class U>
    minimal_allocator(const minimal_allocator<U>& other) noexcept {};

    constexpr T* allocate(size_type size) {
        return (T*) ::operator new (size);
    }

    constexpr void deallocate(T* ptr, size_type size) {
        ::operator delete((void *) ptr);
    }
};


struct Record {
    unsigned start{};
    unsigned end{};
};

template<size_t MAX_ITEMS>
std::ostream& operator<<(std::ostream& os, const std::array<Record, MAX_ITEMS>& arr) {
    for(auto& item : arr) {
        os << "(" << item.start << "," << item.end << ") ";
    }
    return os;
}

template<size_t BUFFER_SIZE, size_t MAX_ITEMS = 100>
class Arena {
public:
    explicit Arena() {
        for(auto& item : records) item = Record{};
    };

    Arena(const Arena& other) = delete;

    Arena(Arena&& other) = delete;

    void _insert_record(unsigned position, unsigned start, unsigned end) {
        for(int i = allocated; i > position; i--) {
            records[i] = records[i-1];
        }
        records[position] = Record{start, end};
        allocated++;
    }

    void* alloc(unsigned size) {
        if(allocated == MAX_ITEMS) {
            throw alloc_failed{"Max items reached"};
        }

        for(unsigned i = 0; i <= allocated; i++) {
            auto last_end = i == 0 ? 0 : records[i - 1].end;
            auto next_start = i < allocated ? records[i].start : BUFFER_SIZE;
            if (size <= (next_start - last_end)) {
                _insert_record(i, last_end, last_end + size);
                return static_cast<void*>(buffer + last_end);
            }
        }

        throw alloc_failed{"No space left for " + std::to_string(size) + " bytes"};
    }

    void dealloc(void* position, unsigned size) noexcept {
        auto location = static_cast<unsigned char*>(position) - buffer;
        for(unsigned i = 0; i < allocated; i++) {
            if(records[i].start == location) {
                assert(size == (records[i].end - records[i].start));
                for(; i < allocated - 1; i++) {
                    records[i] = records[i+1];
                }
                records[allocated - 1] = Record{};
                allocated--;
                return;
            }
        }
        assert(false);
    }

private:
    unsigned char buffer[BUFFER_SIZE];
    std::array<Record, MAX_ITEMS> records{};
    unsigned allocated{};
};


template<class T>
class arena_allocator {
public:
    using value_type = T;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    using propagate_on_container_move_assignment = std::true_type;
    using is_always_equal = std::false_type;

    arena_allocator() : arena{std::make_shared<Arena<10000>>()} {};

    template<class U>
    arena_allocator(const arena_allocator<U>& other) noexcept : arena{other.arena} {};

    constexpr T* allocate(size_type size) {
        return static_cast<T*>(arena->alloc(size * sizeof(T)));
    }

    constexpr void deallocate(T* ptr, size_type size) {
        arena->dealloc(static_cast<void*>(ptr), size * sizeof(T));
    }

private:
    std::shared_ptr<Arena<10000>> arena;

    template<class U>
    friend class arena_allocator;

    template<class U>
    friend bool operator==(const arena_allocator<U>& left, const arena_allocator<U>& right);
};

template<class T>
bool operator==(const arena_allocator<T>& left, const arena_allocator<T>& right) {
    return left.arena == right.arena;
}