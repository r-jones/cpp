#ifndef UTIL_H
#define UTIL_H

#include <type_traits>

template<class T>
constexpr
typename std::enable_if<std::is_move_constructible_v<T> && std::is_move_assignable_v<T>, void>::type
swap(T& lhs, T& rhs) noexcept (
    std::is_nothrow_move_constructible_v<T> &&
    std::is_nothrow_move_assignable_v<T>)
{
    T tmp = std::move(lhs);
    lhs = std::move(rhs);
    rhs = std::move(tmp);
}

template<class T, class U>
constexpr
typename std::enable_if<std::is_move_constructible_v<T> && std::is_assignable_v<T&, U>, T>::type
exchange(T& lhs, U&& rhs) noexcept (
    std::is_nothrow_move_constructible_v<T> &&
    std::is_nothrow_assignable_v<T&, U>)
{
    T tmp = std::move(lhs);
    lhs = std::forward<U>(rhs);
    return tmp;
}

template<class InputIt>
constexpr
typename std::enable_if<std::is_move_constructible_v<typename std::iterator_traits<InputIt>::value_type>
        && std::is_move_assignable_v<typename std::iterator_traits<InputIt>::value_type>, void>::type
iter_swap(InputIt lhs, InputIt rhs) {
    swap(*lhs, *rhs);
}

#endif // UTIL_H