#ifndef LIST_H
#define LIST_H

#include <memory>



template<class T, class Allocator = std::allocator<T>>
class list {
public:

    struct _node {
        T value;
        _node* next{};
        _node* prev{};
    };

    struct _iterator {
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using difference_type = std::ptrdiff_t;
        using pointer = value_type*;
        using reference = value_type&;

        explicit _iterator(_node* ptr = nullptr, _node* prev = nullptr) : ptr{ptr}, prev{prev} {};

        _iterator& operator++() {
            if(ptr != nullptr) {
                prev = ptr;
                ptr = ptr->next;
            } else if(prev != nullptr) {
                prev = nullptr;
                ptr = prev;
            }
            return *this;
        }

        _iterator& operator--() {
            if(ptr != nullptr) {
                prev = ptr;
                ptr = ptr->prev;
            } else if(prev != nullptr) {
                prev = nullptr;
                ptr = prev;
            }
            return *this;
        }

        _iterator operator++(int) {
            _iterator it = *this;
            ++(*this);
            return it;
        }

        _iterator operator--(int) {
            _iterator it = *this;
            --(*this);
            return it;
        }

        value_type& operator*() const {
            return ptr->value;
        }

        bool operator==(const _iterator& other) const {
            return ptr == other.ptr;
        }

//    private:
        _node* ptr;
        _node* prev;
    };

    using value_type = T;
    using allocator_type = Allocator;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference	= value_type&;
    using const_reference = const value_type&;
    using pointer = typename std::allocator_traits<Allocator>::pointer;
    using const_pointer = typename std::allocator_traits<Allocator>::const_pointer;

    using iterator = _iterator;
    using const_iterator = const _iterator;
    using reverse_iterator= std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    list() : alloc{Allocator()}, node_alloc{alloc} {}

    explicit list( const Allocator& alloc ) : alloc{alloc}, node_alloc{alloc}  {}

    list( size_type count,
          const T& value,
          const Allocator& alloc = Allocator()) : alloc{alloc}, node_alloc{alloc} {
        _assign(_same_value_iterator{value, 0}, _same_value_iterator{value, count});
    }

    explicit list( size_type count, const Allocator& alloc = Allocator() ) : list(alloc) {
        while(count-- > 0) {
            emplace_back();
        }
    }

    template< std::input_iterator InputIt >
    list( InputIt first, InputIt last,
          const Allocator& alloc = Allocator() ) : list(alloc) {
        _assign(first, last);
    }

    list( const list& other ) : list(other.begin(), other.end(),
                                     alloc_traits::select_on_container_copy_construction(other.get_allocator())) {}

    list( const list& other, const Allocator& alloc ) : list(other.begin(), other.end(), alloc) {};

    list( list&& other ) : head{other.head}, tail{other.tail}, n{other.n}, alloc{std::move(other.alloc)} {
        node_alloc = n_alloc_type{alloc};
        other.head = nullptr;
        other.tail = nullptr;
        other.n = 0;
    };

    list( list&& other, const Allocator& alloc ) : alloc{alloc}, node_alloc{alloc} {
        if(alloc == other.get_allocator()) {
            head = other.head;
            tail = other.tail;
            n = other.n;
        } else {
            _assign(std::make_move_iterator(other.begin()), std::make_move_iterator(other.end()));
        }
        other.head = nullptr;
        other.tail = nullptr;
        other.n = 0;
    }

    list( std::initializer_list<T> init,
          const Allocator& alloc = Allocator() ) : list(alloc) {
        _assign(init.begin(), init.end());
    }

    ~list() {
        clear();
    }

    list& operator=( const list& other ) {
        if(this == &other) return *this;
        if(std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value &&
            alloc != other.get_allocator()) {
            clear();
            alloc = other.get_allocator();
            node_alloc = n_alloc_type{alloc};
        }
        _assign(other.begin(), other.end());
        return *this;
    }

    list& operator=( list&& other ) { // noexcept(/* see below */) {
        if(this == &other) return *this;
        if(!std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value &&
            alloc != other.get_allocator()) {
            _assign(std::make_move_iterator(other.begin()), std::make_move_iterator(other.end()));
        } else {
            clear();
            if(alloc != other.get_allocator()) {
                alloc = other.get_allocator();
                node_alloc = n_alloc_type{alloc};
            }
            head = other.head;
            tail = other.tail;
            n = other.n;
        }
        other.head = nullptr;
        other.tail = nullptr;
        other.n = 0;
        return *this;
    }

    list& operator=( std::initializer_list<T> ilist ) {
        _assign(ilist.begin(), ilist.end());
        return *this;
    }

    void assign( size_type count, const T& value ) {
        _assign(_same_value_iterator{value, 0}, _same_value_iterator{value, count});
    }

    template< std::input_iterator InputIt >
    void assign( InputIt first, InputIt last ) {
        _assign(first, last);
    }

    void assign( std::initializer_list<T> ilist ) {
        _assign(ilist.begin(), ilist.end());
    }

    allocator_type get_allocator() const noexcept {
        return alloc;
    }

    // Element Access

    reference front() {
        return head->value;
    }

    const_reference front() const {
        return head->value;
    }

    reference back() {
        return tail->value;
    }

    const_reference back() const {
        return tail->value;
    }

    // Iterators
    iterator begin() noexcept {
        return _iterator{head, nullptr};
    }

    const_iterator begin() const noexcept {
        return _iterator{head, nullptr};
    }

    const_iterator cbegin() const noexcept {
        return _iterator{head, nullptr};
    }

    iterator end() noexcept {
        return _iterator{nullptr, tail};
    }

    const_iterator end() const noexcept {
        return _iterator{nullptr, tail};
    }

    const_iterator cend() const noexcept {
        return _iterator{nullptr, tail};
    }

    // Capacity

    [[nodiscard]] bool empty() const noexcept {
        return n == 0;
    }

    size_type size() const noexcept {
        return n;
    }

    size_type max_size() const noexcept {
        return 0;
    }

    // Modifiers

    void clear() noexcept {
        _clear_from(head);
        head = nullptr;
        tail = nullptr;
        n = 0;
    }

    iterator insert( const_iterator pos, const T& value ) {
        return emplace(pos, value);
    }

    iterator insert( const_iterator pos, T&& value ) {
        return emplace(pos, std::move(value));
    }

    iterator insert( const_iterator pos, size_type count, const T& value ) {
        return _insert(pos, _same_value_iterator{value, 0}, _same_value_iterator{value, count});
    }

    template< std::input_iterator InputIt >
    iterator insert( const_iterator pos,
                     InputIt first, InputIt last ) {
        return _insert(pos, first, last);
    }

    iterator insert( const_iterator pos, std::initializer_list<T> ilist ) {
        return _insert(pos, ilist.begin(), ilist.end());
    }

    template< class... Args >
    iterator emplace( const_iterator pos, Args&&... args ) {
        auto node = n_alloc_traits::allocate(node_alloc, 1);
        n_alloc_traits::construct(node_alloc, node, std::forward<Args>(args)...);

        node->next = pos.ptr;
        node->prev = pos.prev;

        if(pos.prev != nullptr) {
            pos.prev->next = node;
        }

        if(pos.ptr != nullptr) {
            pos.ptr->prev = node;
        }

        if(tail == nullptr) {
            assert(n == 0);
            tail = node;
        } else if(tail->next != nullptr) {
            tail = tail->next;
        }

        if(head == nullptr) {
            assert(n == 0);
            head = node;
        } else if(head->prev != nullptr) {
            head = head->prev;
        }

        n++;
        return _iterator{node, node->prev};
    }

    iterator erase( const_iterator pos ) {
        auto tmp = pos.ptr;
        if(tmp != nullptr) {
            if(tmp->prev != nullptr) {
                tmp->prev->next = tmp->next;
            }
            _node* next_prev = nullptr;
            if(tmp->next != nullptr) {
                tmp->next->prev = tmp->prev;
                next_prev = tmp->next->prev;
            }
            auto it = _iterator{tmp->next, next_prev};
            _delete_node(tmp);
            n--;
            return it;
        }
        return end();
    }

    iterator erase( const_iterator first, const_iterator last ) {
        iterator f = first;
        while(f != last) {
            f = erase(f);
        }
        return last;
    }

    void push_front( const T& value ) {
        emplace(begin(), value);
    }

    void push_front( T&& value ) {
        emplace(begin(), std::move(value));
    }

    template< class... Args >
    reference emplace_front( Args&&... args ) {
        emplace(begin(), std::forward<Args>(args)...);
        return head->value;
    }

    void pop_front() {
        if(head != nullptr) {
            auto tmp = head;
            head = head->next;
            if(head != nullptr) {
                head->prev = nullptr;
            }
            _delete_node(tmp);
            n--;
            if(n == 0) {
                tail = nullptr;
            }
        }
    }

    void push_back( const T& value ) {
        emplace(end(), value);
    }

    void push_back( T&& value ) {
        emplace(end(), std::move(value));
    }

    template< class... Args >
    reference emplace_back( Args&&... args ) {
        emplace(end(), std::forward<Args>(args)...);
        return tail->value;
    }

    void pop_back() {
        if(tail != nullptr) {
            auto tmp = tail;
            tail = tail->prev;
            if(tail != nullptr) {
                tail->next = nullptr;
            }
            _delete_node(tmp);
            n--;
            if(n == 0) {
                head = nullptr;
            }
        }
    }

//    void resize( size_type count, const value_type& value );
//
//    void swap( list& other ) noexcept(/* see below */);
//
//    // Operations
//
//    void merge( list& other );
//
//    void merge( list&& other );
//
//    template <class Compare>
//    void merge( list& other, Compare comp );
//
//    template <class Compare>
//    void merge( list&& other, Compare comp );
//
//    void splice( const_iterator pos, list& other );
//
//    void splice( const_iterator pos, list&& other );
//
//    void splice( const_iterator pos, list& other, const_iterator it );
//
//    void splice( const_iterator pos, list&& other, const_iterator it );
//
//    void splice( const_iterator pos, list& other,
//                 const_iterator first, const_iterator last);
//
//    void splice( const_iterator pos, list&& other,
//                 const_iterator first, const_iterator last );
//
//    size_type remove( const T& value );
//
//    template< class UnaryPredicate >
//    size_type remove_if( UnaryPredicate p );
//
//    void reverse() noexcept;
//
//    size_type unique();
//
//    template< class BinaryPredicate >
//    size_type unique( BinaryPredicate p );
//
//    void sort();
//
//    template< class Compare >
//    void sort( Compare comp );

private:
    using alloc_traits = std::allocator_traits<Allocator>;
    using n_alloc_type = typename std::allocator_traits<Allocator>::rebind_alloc<_node>;
    using n_alloc_traits = std::allocator_traits<n_alloc_type>;

    // TODO: Make this into an input iterator!
    struct _same_value_iterator {
        const T& value;
        size_type pos{};
        _same_value_iterator operator++(int) {
            _same_value_iterator it = *this;
            pos++;
            return it;
        }
        _same_value_iterator operator--(int) {
            _same_value_iterator it = *this;
            pos--;
            return it;
        }
        const T& operator*() {
            return value;
        }
        bool operator==(const _same_value_iterator& other) const {
            return pos == other.pos;
        }
    };

    void _clear_from(_node* node) {
        while(node != nullptr) {
            auto node_next = node->next;
            _delete_node(node);
            node = node_next;
        }
    }

    void _delete_node(_node* node) {
        n_alloc_traits::destroy(node_alloc, node);
        n_alloc_traits::deallocate(node_alloc, node, 1);
    }

    template<class InputIt>
    void _assign(InputIt first, InputIt last) {
        n = 0;
        tail = head;
        _node* prev = nullptr;

        while(first != last) {
            if(tail == nullptr) {
                tail = n_alloc_traits::allocate(node_alloc, 1);
                n_alloc_traits::construct(node_alloc, tail, *first);
                if(head == nullptr) {
                    head = tail;
                }
            } else {
                tail->value = *first;
            }

            if(prev != nullptr) {
                prev->next = tail;
                tail->prev = prev;
            }

            prev = tail;
            tail = tail->next;
            first++;
            n++;
        }
        _clear_from(tail);
        tail = prev;
        if(tail != nullptr) {
            tail->next = nullptr;
        }
    }

    template<class InputIt>
    iterator _insert( const_iterator pos,
                  InputIt first, InputIt last ) {
        iterator it = pos;
        while (last-- != first) {
            it = insert(it, *last);
        }
        return it;
    }

    _node* head{};
    _node* tail{};
    size_t n{};
    Allocator alloc;
    n_alloc_type node_alloc;
};

template< class T, class Alloc >
bool operator==( const list<T,Alloc>& lhs, const list<T,Alloc>& rhs ) {
    if(lhs.size() != rhs.size()) return false;
    auto lit = lhs.begin();
    auto rit = rhs.begin();
    while(lit != lhs.end()) {
        if(*lit != *rit) return false;
        lit++;
        rit++;
    }
    return true;
}

#endif // LIST_H