#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <cstddef>
#include <memory>
#include <stdexcept>
#include <cstring>

template<typename T>
struct pointer_iterator {
    using iterator_category = std::input_iterator_tag;
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = value_type*;
    using reference = value_type&;

    explicit pointer_iterator(pointer p) : p{p} {};

    pointer_iterator& operator++() {
        p++;
        return *this;
    }

    pointer_iterator& operator--() {
        p--;
        return *this;
    }

    pointer_iterator operator++(int) {
        const pointer_iterator it = *this;
        p++;
        return it;
    }

    pointer_iterator operator--(int) {
        const pointer_iterator it = *this;
        p--;
        return it;
    }

    pointer_iterator operator+(int x) const {
        pointer_iterator it = *this;
        it.p += x;
        return it;
    }

    pointer_iterator operator-(int x) const {
        pointer_iterator it = *this;
        it.p -= x;
        return it;
    }

    int operator-(pointer_iterator x) const {
        return p - x.p;
    }

    value_type& operator*() {
        return *p;
    }

    // TODO: Understand why:
    //       const value_type& operator*() const
    // is not required for:
    //      std::ostream& operator<<(std::ostream& os, const vector<T>& vec)
    //
    //    const value_type& operator*() const {
    //        return *p;
    //    }

    bool operator==(const pointer_iterator& other) const {
        return p == other.p;
    }

    bool operator<(const pointer_iterator& other) const {
        return p < other.p;
    }

    bool operator<=(const pointer_iterator& other) const {
        return p <= other.p;
    }

    bool operator>(const pointer_iterator& other) const {
        return p > other.p;
    }

    bool operator>=(const pointer_iterator& other) const {
        return p >= other.p;
    }

private:
    pointer p;
};


template<class T, class Allocator = std::allocator<T>>
class vector {
public:
    using value_type = T;
    using allocator_type = Allocator;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = typename std::allocator_traits<Allocator>::pointer;
    using const_pointer = typename std::allocator_traits<Allocator>::const_pointer;

    using iterator = pointer_iterator<T>;
    using const_iterator = const pointer_iterator<T>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    constexpr vector() noexcept(noexcept(Allocator())) = default;

    constexpr explicit vector(const Allocator& alloc) noexcept : allocator{alloc} {};

    constexpr vector(size_type count, const T& value, const Allocator& alloc = Allocator())
            : n{count}, allocator{alloc} {
        allocate(n);
        fill_from_value(iterator{p}, value, n);
    };

    constexpr explicit vector(size_type count, const Allocator& alloc = Allocator())
            : n{count}, allocator{alloc} {
        allocate(n);
        default_construct_items(iterator{p}, n);
    }

    template<std::input_iterator InputIt>
    constexpr vector(InputIt first, InputIt last, const Allocator& alloc = Allocator()) : allocator{alloc} {
        n = std::distance(first, last);
        allocate(n);
        fill_from_iterator(iterator{p}, first, n);
    };

    constexpr vector(const vector& other, const Allocator& alloc) : n{other.n}, allocator{alloc} {
        allocate(n);
        for(size_type i = 0; i < n; i++) {
            std::construct_at(p + i, other[i]);
        }
    };

    constexpr vector(const vector& other)
            : vector{other, std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.allocator)} {}

    constexpr vector(vector&& other) noexcept : p{other.p}, n{other.n}, allocator{std::move(other.allocator)} {
        other.p = nullptr;
        other.n = 0;
    };

    constexpr vector(vector&& other, const Allocator& alloc) : p{other.p}, n{other.n}, allocator{alloc} {
        if(allocator == other.allocator) {
            p = other.p;
            n = other.n;
        } else {
            allocate(n);
            for(size_type i = 0; i < n; i++) {
                std::construct_at(p + i, std::move(other[i]));
            }
        }
        other.p = nullptr;
        other.n = 0;
    };

    constexpr vector(std::initializer_list<T> init, const Allocator& alloc = Allocator()) : n{init.size()}, allocator{alloc} {
        allocate(n);
        fill_from_iterator(iterator{p}, init.begin(), n);
    }

    constexpr ~vector() {
        clean_up();
    }

    constexpr vector& operator=(const vector& other) {
        move_or_copy_from_existing(other);
        return *this;
    }

    // TODO: No except
    constexpr vector& operator=(vector&& other) { //noexcept(/* see below */) {
        move_or_copy_from_existing(std::move(other));
        return *this;
    }

    constexpr vector& operator=(std::initializer_list<T> init) {
        assign(init);
        return *this;
    }

    constexpr void assign(size_type count, const T& value) {
        if(count > allocated_size) {
            clean_up();
            allocate(count);
            fill_from_value(iterator{p}, value, count);
        } else if(n > count) {
            std::destroy_n(p + count, n - count);
            for(size_type i = 0; i < count; i++) {
                *(p + i) = value;
            }
        }
        n = count;
    }

    template<std::input_iterator InputIt>
    constexpr void assign(InputIt first, InputIt last) {
        size_type count = std::distance(first, last);
        if(count > allocated_size) {
            clean_up();
            allocate(count);
            fill_from_iterator(iterator{p}, first, count);
        } else if(n > count) {
            std::destroy_n(p + count, n - count);
            for(size_type i = 0; i < count; i++) {
                *(p + i) = *first++;
            }
        }
        n = count;
    }

    constexpr void assign(std::initializer_list<T> init) {
        assign(init.begin(), init.end());
    }

    constexpr allocator_type get_allocator() const noexcept {
        return allocator;
    }

    // Element Access

    constexpr reference at(size_type pos) {
        check_bounds(pos);
        return (*this)[pos];
    }

    constexpr const_reference at(size_type pos) const {
        check_bounds(pos);
        return (*this)[pos];
    }

    constexpr reference operator[](size_type pos) {
        return p[pos];
    }

    constexpr const_reference operator[](size_type pos) const {
        return p[pos];
    }

    constexpr reference front() {
        return p[0];
    }

    constexpr const_reference front() const {
        return p[0];
    }

    constexpr reference back() {
        return p[n - 1];
    }

    constexpr const_reference back() const {
        return p[n - 1];
    }

    constexpr pointer data() noexcept {
        return p;
    }

    constexpr const pointer data() const noexcept {
        return p;
    }

    // Iterators

    constexpr iterator begin() noexcept {
        return iterator{p};
    }

    constexpr const_iterator begin() const noexcept {
        return iterator{p};
    }

    constexpr const_iterator cbegin() const noexcept {
        return iterator{p};
    }

    constexpr iterator end() noexcept {
        return iterator{p + n};
    }

    constexpr const_iterator end() const noexcept {
        return iterator{p + n};
    }

    constexpr const_iterator cend() const noexcept {
        return iterator{p + n};
    }

    constexpr reverse_iterator rbegin() noexcept {
        return std::make_reverse_iterator(end());
    }

    constexpr const_reverse_iterator crbegin() const noexcept {
        return std::make_reverse_iterator(cend());
    }

    constexpr reverse_iterator rend() noexcept {
        return std::make_reverse_iterator(begin());
    }

    constexpr const_reverse_iterator crend() const noexcept {
        return std::make_reverse_iterator(cbegin());
    }

    // Capacity

    [[nodiscard]] constexpr bool empty() const noexcept {
        return n == 0;
    }

    constexpr size_type size() const noexcept {
        return n;
    }

    constexpr size_type max_size() const noexcept {
        // Don't know how to implement this
        return 0;
    }

    constexpr void reserve(size_type new_cap) {
        if(new_cap > allocated_size) {
            allocate_new_and_copy_existing_data(new_cap);
        }
    }

    constexpr size_type capacity() const noexcept {
        return allocated_size;
    }

    constexpr void shrink_to_fit() {
        if(allocated_size > n) {
            allocate_new_and_copy_existing_data(n);
        }
    }

    // Modifiers

    constexpr void clear() noexcept {
        std::destroy_n(p, n);
        n = 0;
    }

    constexpr iterator insert(const_iterator pos, const T& value) {
        return insert_copy_or_move(pos, 1, value);
    }

    constexpr iterator insert(const_iterator pos, T&& value) {
        return insert_copy_or_move(pos, 1, std::move(value));
    }

    constexpr iterator insert(const_iterator pos, size_type count, const T& value) {
        return insert_copy_or_move(pos, count, value);
    }

    template<std::input_iterator InputIt>
    constexpr iterator insert(const_iterator pos, InputIt first, InputIt last) {
        size_type count = std::distance(first, last);
        iterator new_pos = prepare_for_insert(pos, count);
        fill_from_iterator(new_pos, first, count);
        return new_pos;
    }

    constexpr iterator insert(const_iterator pos, std::initializer_list<T> init) {
        iterator new_pos = prepare_for_insert(pos, init.size());
        fill_from_iterator(new_pos, init.begin(), init.size());
        return new_pos;
    }

    template<class... Args>
    constexpr iterator emplace(const_iterator pos, Args&&... args) {
        iterator it = prepare_for_insert(pos, 1);
        std::construct_at(&(*it), std::forward<Args>(args)...);
        return it;
    }

    constexpr iterator erase(const_iterator pos) {
        return erase_n(pos, 1);
    }

    constexpr iterator erase(const_iterator first, const_iterator last) {
        return erase_n(first, std::distance(first, last));
    }

    constexpr void push_back(const T& value) {
        push_back_copy_or_move(value);
    }

    constexpr void push_back(T&& value) {
        push_back_copy_or_move(std::move(value));
    }

    template<class... Args>
    constexpr reference emplace_back(Args&&... args) {
        increase_capacity_if_required(1);
        std::construct_at(p + n, std::forward<Args>(args)...);
        n++;
        return *(p + n);
    }

    constexpr void pop_back() {
        std::destroy_n(p + n - 1, 1);
        n--;
    }

    // TODO: Can we unify the resize functions?
    constexpr void resize(size_type count) {
        _resize(count, nullptr);
    }

    constexpr void resize(size_type count, const value_type& value) {
        _resize(count, &value);
    }

    // If std::allocator_traits<allocator_type>::propagate_on_container_swap::value is true,
    // then the allocators are exchanged using an unqualified call to non-member swap.
    // Otherwise, they are not swapped (and if get_allocator() != other.get_allocator(), the behavior is undefined).

    constexpr void swap(vector& other) { // noexcept(/* see below */) {
        std::swap(p, other.p);
        std::swap(n, other.n);
        std::swap(allocated_size, other.allocated_size);

        if(std::allocator_traits<allocator_type>::propagate_on_container_swap::value) {
            std::swap(allocator, other.allocator);
        }
    }

private:

    constexpr void _resize(size_type count, const value_type* value_ptr) {
        if(count <= n) {
            std::destroy_n(p + count, n - count);
        } else {
            increase_capacity_if_required(count - n);
            if(value_ptr) {
                fill_from_value(iterator{p + n}, *value_ptr, count - n);
            } else {
                default_construct_items(iterator{p + n}, count - n);
            }
        }
        n = count;
    }

    // TODO: Very similar to assign!
    template<typename V>
    constexpr void move_or_copy_from_existing(V&& other) {
        constexpr bool is_move = std::is_rvalue_reference_v<decltype(std::forward<V>(other))>;
        constexpr bool propagate_allocator =
                is_move ?
                std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value :
                std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value;

        if(propagate_allocator && other.allocator != allocator) {

            clean_up();
            allocator = other.allocator;
            n = other.n;
            allocate(n);

            for(size_t i = 0; i < n; i++) {
                std::construct_at(p + i, is_move ? std::move(other[i]) : other[i]);
            }

        } else {
            auto first = other.begin();
            if(other.n > allocated_size) {
                clean_up();
                allocate(other.n);
                if (is_move) {
                    move_from_iterator(iterator{p}, first, other.n);
                } else {
                    fill_from_iterator(iterator{p}, first, other.n);
                }
            } else if(n > other.n) {
                std::destroy_n(p + other.n, n - other.n);
                for(size_type i = 0; i < other.n; i++) {
                    // cond ? foo : bar, evaluates both foo and bar.
                    // *(p + i) = is_move ? std::move(*first++) : *first++;
                    if(is_move) {
                        *(p + i) = std::move(*first++);
                    } else {
                        *(p + i) = *first++;
                    }
                }
            }
            n = other.n;
        }
    }

    constexpr void reallocate_or_destroy_n(size_type new_size) {
        if(new_size > allocated_size) {
            clean_up();
            allocate(new_size);
        } else if(n > new_size) {
            std::destroy_n(p + new_size, n - new_size);
        }
        n = new_size;
    }

    template<class InputIt>
    constexpr void fill_from_iterator(iterator pos, InputIt first, size_type count) {
        for(size_type i = 0; i < count; i++) {
            std::construct_at(&(*(pos + i)), *first++);
        }
    }

    template<class InputIt>
    constexpr void move_from_iterator(iterator pos, InputIt first, size_type count) {
        for(size_type i = 0; i < count; i++) {
            std::construct_at(&(*(pos + i)), std::move(*first++));
        }
    }

    constexpr void default_construct_items(iterator pos, size_type count) {
        for(size_type i = 0; i < count; i++) {
            std::construct_at<T>(&(*(pos + i)));
        }
    }

    constexpr void fill_from_value(iterator pos, const T& value, size_type count) {
        for(size_type i = 0; i < count; i++) {
            std::construct_at(&(*(pos + i)), value);
        }
    }

    void allocate(size_type count) {
        allocated_size = count;
        p = allocator.allocate(count);
    }

    void clean_up() {
        std::destroy_n(p, n);
        allocator.deallocate(p, allocated_size);
        n = 0;
        allocated_size = 0;
    }

    constexpr inline void check_bounds(size_type pos) {
        if(pos >= n) {
            throw std::out_of_range{"Index " + std::to_string(pos) + " out of bounds"};
        }
    }

    constexpr void allocate_new_and_copy_existing_data(size_type new_size) {
        auto new_p = allocator.allocate(new_size);
        std::memcpy(new_p, p, n * sizeof(T));
        allocator.deallocate(p, allocated_size);
        p = new_p;
        allocated_size = new_size;
    }

    constexpr void increase_capacity_if_required(size_type insert_count = 1) {
        if (n + insert_count > allocated_size) {
            allocate_new_and_copy_existing_data(allocated_size * 2);
        }
    }

    template<class P>
    constexpr iterator insert_copy_or_move(const_iterator pos, size_type count, P&& value) {
        iterator new_pos = prepare_for_insert(pos, count);
        for(auto it = new_pos; it < new_pos + count; ++it) {
            std::construct_at(&(*(it)), std::forward<P>(value));
        }
        return new_pos;
    }

    constexpr const_iterator prepare_for_insert(const_iterator pos, size_type count) {
        auto distance = pos - begin();
        auto to_move_assign = end() - pos - count;

        increase_capacity_if_required(count);
        n += count;

        const_iterator new_pos = begin() + distance;

        for(size_type i = 1; i <= count; i++) {
            auto move_to = p + n - i;
            std::construct_at(move_to, std::move(*(move_to - count)));
        }

        for(size_type i = 1; i <= to_move_assign; i++) {
            auto move_to = p + n - i - count;
            *move_to = std::move(*(move_to - count));
        }

        return new_pos;
    }

    constexpr void shift_elements_after_delete(const_iterator pos, size_type count) {
        for(auto it = pos; it < end() - count; ++it) {
            *it = std::move(*(it + count));
        }
    }

    constexpr iterator erase_n(const_iterator pos, size_type count) {
        std::destroy_n(pos, count);
        shift_elements_after_delete(pos, count);
        n -= count;
        return pos;
    }

    template<typename P>
    constexpr void push_back_copy_or_move(P&& value) {
        increase_capacity_if_required(1);
        std::construct_at(p + n, std::forward<P>(value));
        n++;
    }

    pointer p{};
    size_type n{};

    allocator_type allocator{};
    size_type allocated_size{};
};

template<typename T>
constexpr bool operator==(const vector<T>& left, const vector<T>& right) {
    if(left.size() != right.size()) return false;
    for(size_t i = 0; i < left.size(); i++) {
        if(left[i] != right[i]) return false;
    }
    return true;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const vector<T>& vec) {
    for(const auto item : vec) os << item << " ";
    return os;
}

#endif // VECTOR_H